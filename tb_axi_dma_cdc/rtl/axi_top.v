module axi_top (  /*autoarg*/
   // Outputs
   s_axis_write_desc_ready, s_axis_write_data_tready,
   s_axis_read_desc_ready, s_axis_desc_ready,
   m_axis_write_desc_status_valid, m_axis_write_desc_status_user,
   m_axis_write_desc_status_tag, m_axis_write_desc_status_len,
   m_axis_write_desc_status_id, m_axis_write_desc_status_error,
   m_axis_write_desc_status_dest, m_axis_read_desc_status_valid,
   m_axis_read_desc_status_tag, m_axis_read_desc_status_error,
   m_axis_read_data_tvalid, m_axis_read_data_tuser,
   m_axis_read_data_tlast, m_axis_read_data_tkeep,
   m_axis_read_data_tid, m_axis_read_data_tdest,
   m_axis_read_data_tdata, m_axis_desc_status_valid,
   m_axis_desc_status_tag, m_axis_desc_status_error,
   // Inputs
   write_enable, write_abort, s_axis_write_desc_valid,
   s_axis_write_desc_tag, s_axis_write_desc_len,
   s_axis_write_desc_addr, s_axis_write_data_tvalid,
   s_axis_write_data_tuser, s_axis_write_data_tlast,
   s_axis_write_data_tkeep, s_axis_write_data_tid,
   s_axis_write_data_tdest, s_axis_write_data_tdata,
   s_axis_read_desc_valid, s_axis_read_desc_user,
   s_axis_read_desc_tag, s_axis_read_desc_len, s_axis_read_desc_id,
   s_axis_read_desc_dest, s_axis_read_desc_addr,
   s_axis_desc_write_addr, s_axis_desc_valid, s_axis_desc_tag,
   s_axis_desc_read_addr, s_axis_desc_len, rst, read_enable,
   m_axis_read_data_tready, enable, clk1, clk
   );

   localparam S_COUNT = 2;
   localparam M_COUNT = 1;

    //// Width of data bus in bits
    //parameter AXI_DATA_WIDTH = 32;
    //// Width of address bus in bits
    //parameter AXI_ADDR_WIDTH = 16;
    //// Width of wstrb (width of data bus in words)
    //parameter AXI_STRB_WIDTH = (AXI_DATA_WIDTH/8);
    //// Width of AXI ID signal
    //parameter AXI_ID_WIDTH = 8;
    // Maximum AXI burst length to generate
    //parameter AXI_MAX_BURST_LEN = 16;
    // Width of length field
    //parameter LEN_WIDTH = 20;
    // Width of tag field
    //parameter TAG_WIDTH = 8;
    // Enable support for unaligned transfers
    //parameter ENABLE_UNALIGNED = 0

   // Width of AXI data bus in bits
    parameter AXI_DATA_WIDTH = 512;
    // Width of AXI address bus in bits
    parameter AXI_ADDR_WIDTH = 32;
    // Width of AXI wstrb (width of data bus in words)
    parameter AXI_STRB_WIDTH = (AXI_DATA_WIDTH/8);
    // Width of AXI ID signal
    parameter AXI_ID_WIDTH = 8;
    // Maximum AXI burst length to generate
    parameter AXI_MAX_BURST_LEN = 16;
    // Width of AXI stream interfaces in bits
    parameter AXIS_DATA_WIDTH = AXI_DATA_WIDTH;
    // Use AXI stream tkeep signal
    parameter AXIS_KEEP_ENABLE = (AXIS_DATA_WIDTH>8);
    // AXI stream tkeep signal width (words per cycle)
    parameter AXIS_KEEP_WIDTH = (AXIS_DATA_WIDTH/8);
    // Use AXI stream tlast signal
    parameter AXIS_LAST_ENABLE = 1;
    // Propagate AXI stream tid signal
    parameter AXIS_ID_ENABLE = 0;
    // AXI stream tid signal width
    parameter AXIS_ID_WIDTH = 7;
    // Propagate AXI stream tdest signal
    parameter AXIS_DEST_ENABLE = 0;
    // AXI stream tdest signal width
    parameter AXIS_DEST_WIDTH = 8;
    // Propagate AXI stream tuser signal
    parameter AXIS_USER_ENABLE = 1;
    // AXI stream tuser signal width
    parameter AXIS_USER_WIDTH = 1;
    // Width of length field
    parameter LEN_WIDTH = 20;
    // Width of tag field
    parameter TAG_WIDTH = 8;
    // Enable support for scatter/gather DMA
    // (multiple descriptors per AXI stream frame)
    parameter ENABLE_SG = 0;
    // Enable support for unaligned transfers
    parameter ENABLE_UNALIGNED = 0 ;


parameter AXI_WORD_WIDTH = AXI_STRB_WIDTH;
parameter AXI_WORD_SIZE = AXI_DATA_WIDTH/AXI_WORD_WIDTH;
parameter AXI_BURST_SIZE = $clog2(AXI_STRB_WIDTH);
parameter AXI_MAX_BURST_SIZE = AXI_MAX_BURST_LEN << AXI_BURST_SIZE;

parameter OFFSET_WIDTH = AXI_STRB_WIDTH > 1 ? $clog2(AXI_STRB_WIDTH) : 1;
parameter OFFSET_MASK = AXI_STRB_WIDTH > 1 ? {OFFSET_WIDTH{1'b1}} : 0;
parameter ADDR_MASK = {AXI_ADDR_WIDTH{1'b1}} << $clog2(AXI_STRB_WIDTH);
parameter CYCLE_COUNT_WIDTH = LEN_WIDTH - AXI_BURST_SIZE + 1;

parameter STATUS_FIFO_ADDR_WIDTH = 5;
parameter OUTPUT_FIFO_ADDR_WIDTH = 5;



   // Width of data bus in bits
    parameter DATA_WIDTH = 512;
    // Width of address bus in bits
    parameter ADDR_WIDTH = 32;
    // Width of wstrb (width of data bus in words)
    parameter STRB_WIDTH = (DATA_WIDTH/8);
    // Input ID field width (from AXI masters)
    parameter S_ID_WIDTH = 8;
    // Output ID field width (towards AXI slaves)
    // Additional bits required for response routing
    parameter M_ID_WIDTH = S_ID_WIDTH+$clog2(S_COUNT);
    // Propagate awuser signal
    parameter AWUSER_ENABLE = 0;
    // Width of awuser signal
    parameter AWUSER_WIDTH = 1;
    // Propagate wuser signal
    parameter WUSER_ENABLE = 0;
    // Width of wuser signal
    parameter WUSER_WIDTH = 1;
    // Propagate buser signal
    parameter BUSER_ENABLE = 0;
    // Width of buser signal
    parameter BUSER_WIDTH = 1;
    // Propagate aruser signal
    parameter ARUSER_ENABLE = 0;
    // Width of aruser signal
    parameter ARUSER_WIDTH = 1;
    // Propagate ruser signal
    parameter RUSER_ENABLE = 0;
    // Width of ruser signal
    parameter RUSER_WIDTH = 1;
    // Number of concurrent unique IDs
    parameter S00_THREADS = 2;
    // Number of concurrent operations
    parameter S00_ACCEPT = 16;
    // Number of concurrent unique IDs
    parameter S01_THREADS = 2;
    // Number of concurrent operations
    parameter S01_ACCEPT = 16;
    // Number of regions per master interface
    parameter M_REGIONS = 1;
    // Master interface base addresses
    // M_REGIONS concatenated fields of ADDR_WIDTH bits
    parameter M00_BASE_ADDR = 0;
    // Master interface address widths
    // M_REGIONS concatenated fields of 32 bits
    parameter M00_ADDR_WIDTH = {M_REGIONS{32'd24}};
    // Read connections between interfaces
    // S_COUNT bits
    parameter M00_CONNECT_READ = 2'b11;
    // Write connections between interfaces
    // S_COUNT bits
    parameter M00_CONNECT_WRITE = 2'b11;
    // Number of concurrent operations for each master interface
    parameter M00_ISSUE = 4;
    // Secure master (fail operations based on awprot/arprot)
    parameter M00_SECURE = 0;
    // Slave interface AW channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_AW_REG_TYPE = 0;
    // Slave interface W channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_W_REG_TYPE = 0;
    // Slave interface B channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_B_REG_TYPE = 1;
    // Slave interface AR channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_AR_REG_TYPE = 0;
    // Slave interface R channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_R_REG_TYPE = 2;
    // Slave interface AW channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_AW_REG_TYPE = 0;
    // Slave interface W channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_W_REG_TYPE = 0;
    // Slave interface B channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_B_REG_TYPE = 1;
    // Slave interface AR channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_AR_REG_TYPE = 0;
    // Slave interface R channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_R_REG_TYPE = 2;
    // Master interface AW channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_AW_REG_TYPE = 1;
    // Master interface W channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_W_REG_TYPE = 2;
    // Master interface B channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_B_REG_TYPE = 0;
    // Master interface AR channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_AR_REG_TYPE = 1;
    // Master interface R channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_R_REG_TYPE = 0;


      // Width of data bus in bits
    //parameter DATA_WIDTH = 32;
    // Width of address bus in bits
    //parameter ADDR_WIDTH = 16;
    // Width of wstrb (width of data bus in words)
    //parameter STRB_WIDTH = (DATA_WIDTH/8);
    // Width of ID signal
    parameter ID_WIDTH = 8;
    // Extra pipeline register on output
    parameter PIPELINE_OUTPUT = 0;

parameter VALID_ADDR_WIDTH = ADDR_WIDTH - $clog2(STRB_WIDTH);
parameter WORD_WIDTH = STRB_WIDTH;
parameter WORD_SIZE = DATA_WIDTH/WORD_WIDTH;


  /*AUTOINPUT*/
  // Beginning of automatic inputs (from unused autoinst inputs)
  input			clk;			// To u00_dma of axi_dma.v, ...
  input			clk1;			// To u03_cdc of axi_cdc_top.v, ...
  input			enable;			// To u01_cdma of axi_cdma.v
  input			m_axis_read_data_tready;// To u00_dma of axi_dma.v
  input			read_enable;		// To u00_dma of axi_dma.v
  input			rst;			// To u00_dma of axi_dma.v, ...
  input [LEN_WIDTH-1:0]	s_axis_desc_len;	// To u01_cdma of axi_cdma.v
  input [AXI_ADDR_WIDTH-1:0] s_axis_desc_read_addr;// To u01_cdma of axi_cdma.v
  input [TAG_WIDTH-1:0]	s_axis_desc_tag;	// To u01_cdma of axi_cdma.v
  input			s_axis_desc_valid;	// To u01_cdma of axi_cdma.v
  input [AXI_ADDR_WIDTH-1:0] s_axis_desc_write_addr;// To u01_cdma of axi_cdma.v
  input [AXI_ADDR_WIDTH-1:0] s_axis_read_desc_addr;// To u00_dma of axi_dma.v
  input [AXIS_DEST_WIDTH-1:0] s_axis_read_desc_dest;// To u00_dma of axi_dma.v
  input [AXIS_ID_WIDTH-1:0] s_axis_read_desc_id;// To u00_dma of axi_dma.v
  input [LEN_WIDTH-1:0]	s_axis_read_desc_len;	// To u00_dma of axi_dma.v
  input [TAG_WIDTH-1:0]	s_axis_read_desc_tag;	// To u00_dma of axi_dma.v
  input [AXIS_USER_WIDTH-1:0] s_axis_read_desc_user;// To u00_dma of axi_dma.v
  input			s_axis_read_desc_valid;	// To u00_dma of axi_dma.v
  input [AXIS_DATA_WIDTH-1:0] s_axis_write_data_tdata;// To u00_dma of axi_dma.v
  input [AXIS_DEST_WIDTH-1:0] s_axis_write_data_tdest;// To u00_dma of axi_dma.v
  input [AXIS_ID_WIDTH-1:0] s_axis_write_data_tid;// To u00_dma of axi_dma.v
  input [AXIS_KEEP_WIDTH-1:0] s_axis_write_data_tkeep;// To u00_dma of axi_dma.v
  input			s_axis_write_data_tlast;// To u00_dma of axi_dma.v
  input [AXIS_USER_WIDTH-1:0] s_axis_write_data_tuser;// To u00_dma of axi_dma.v
  input			s_axis_write_data_tvalid;// To u00_dma of axi_dma.v
  input [AXI_ADDR_WIDTH-1:0] s_axis_write_desc_addr;// To u00_dma of axi_dma.v
  input [LEN_WIDTH-1:0]	s_axis_write_desc_len;	// To u00_dma of axi_dma.v
  input [TAG_WIDTH-1:0]	s_axis_write_desc_tag;	// To u00_dma of axi_dma.v
  input			s_axis_write_desc_valid;// To u00_dma of axi_dma.v
  input			write_abort;		// To u00_dma of axi_dma.v
  input			write_enable;		// To u00_dma of axi_dma.v
  // End of automatics
  /*AUTOOUTPUT*/
  // Beginning of automatic outputs (from unused autoinst outputs)
  output [3:0]		m_axis_desc_status_error;// From u01_cdma of axi_cdma.v
  output [TAG_WIDTH-1:0] m_axis_desc_status_tag;// From u01_cdma of axi_cdma.v
  output		m_axis_desc_status_valid;// From u01_cdma of axi_cdma.v
  output [AXIS_DATA_WIDTH-1:0] m_axis_read_data_tdata;// From u00_dma of axi_dma.v
  output [AXIS_DEST_WIDTH-1:0] m_axis_read_data_tdest;// From u00_dma of axi_dma.v
  output [AXIS_ID_WIDTH-1:0] m_axis_read_data_tid;// From u00_dma of axi_dma.v
  output [AXIS_KEEP_WIDTH-1:0] m_axis_read_data_tkeep;// From u00_dma of axi_dma.v
  output		m_axis_read_data_tlast;	// From u00_dma of axi_dma.v
  output [AXIS_USER_WIDTH-1:0] m_axis_read_data_tuser;// From u00_dma of axi_dma.v
  output		m_axis_read_data_tvalid;// From u00_dma of axi_dma.v
  output [3:0]		m_axis_read_desc_status_error;// From u00_dma of axi_dma.v
  output [TAG_WIDTH-1:0] m_axis_read_desc_status_tag;// From u00_dma of axi_dma.v
  output		m_axis_read_desc_status_valid;// From u00_dma of axi_dma.v
  output [AXIS_DEST_WIDTH-1:0] m_axis_write_desc_status_dest;// From u00_dma of axi_dma.v
  output [3:0]		m_axis_write_desc_status_error;// From u00_dma of axi_dma.v
  output [AXIS_ID_WIDTH-1:0] m_axis_write_desc_status_id;// From u00_dma of axi_dma.v
  output [LEN_WIDTH-1:0] m_axis_write_desc_status_len;// From u00_dma of axi_dma.v
  output [TAG_WIDTH-1:0] m_axis_write_desc_status_tag;// From u00_dma of axi_dma.v
  output [AXIS_USER_WIDTH-1:0] m_axis_write_desc_status_user;// From u00_dma of axi_dma.v
  output		m_axis_write_desc_status_valid;// From u00_dma of axi_dma.v
  output		s_axis_desc_ready;	// From u01_cdma of axi_cdma.v
  output		s_axis_read_desc_ready;	// From u00_dma of axi_dma.v
  output		s_axis_write_data_tready;// From u00_dma of axi_dma.v
  output		s_axis_write_desc_ready;// From u00_dma of axi_dma.v
  // End of automatics

  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  wire [ADDR_WIDTH-1:0]	m00_axi_araddr;		// From u02_arbitor of axi_xbar_2x1.v
  wire [1:0]		m00_axi_arburst;	// From u02_arbitor of axi_xbar_2x1.v
  wire [3:0]		m00_axi_arcache;	// From u02_arbitor of axi_xbar_2x1.v
  wire [M_ID_WIDTH-1:0]	m00_axi_arid;		// From u02_arbitor of axi_xbar_2x1.v
  wire [7:0]		m00_axi_arlen;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_arlock;		// From u02_arbitor of axi_xbar_2x1.v
  wire [2:0]		m00_axi_arprot;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_arready;	// From u03_cdc of axi_cdc_top.v
  wire [2:0]		m00_axi_arsize;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_arvalid;	// From u02_arbitor of axi_xbar_2x1.v
  wire [ADDR_WIDTH-1:0]	m00_axi_awaddr;		// From u02_arbitor of axi_xbar_2x1.v
  wire [1:0]		m00_axi_awburst;	// From u02_arbitor of axi_xbar_2x1.v
  wire [3:0]		m00_axi_awcache;	// From u02_arbitor of axi_xbar_2x1.v
  wire [M_ID_WIDTH-1:0]	m00_axi_awid;		// From u02_arbitor of axi_xbar_2x1.v
  wire [7:0]		m00_axi_awlen;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_awlock;		// From u02_arbitor of axi_xbar_2x1.v
  wire [2:0]		m00_axi_awprot;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_awready;	// From u03_cdc of axi_cdc_top.v
  wire [2:0]		m00_axi_awsize;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_awvalid;	// From u02_arbitor of axi_xbar_2x1.v
  wire [ID_WIDTH-1:0]	m00_axi_bid;		// From u03_cdc of axi_cdc_top.v
  wire			m00_axi_bready;		// From u02_arbitor of axi_xbar_2x1.v
  wire [1:0]		m00_axi_bresp;		// From u03_cdc of axi_cdc_top.v
  wire			m00_axi_bvalid;		// From u03_cdc of axi_cdc_top.v
  wire [DATA_WIDTH-1:0]	m00_axi_rdata;		// From u03_cdc of axi_cdc_top.v
  wire [ID_WIDTH-1:0]	m00_axi_rid;		// From u03_cdc of axi_cdc_top.v
  wire			m00_axi_rlast;		// From u03_cdc of axi_cdc_top.v
  wire			m00_axi_rready;		// From u02_arbitor of axi_xbar_2x1.v
  wire [1:0]		m00_axi_rresp;		// From u03_cdc of axi_cdc_top.v
  wire			m00_axi_rvalid;		// From u03_cdc of axi_cdc_top.v
  wire [DATA_WIDTH-1:0]	m00_axi_wdata;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_wlast;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_wready;		// From u03_cdc of axi_cdc_top.v
  wire [STRB_WIDTH-1:0]	m00_axi_wstrb;		// From u02_arbitor of axi_xbar_2x1.v
  wire			m00_axi_wvalid;		// From u02_arbitor of axi_xbar_2x1.v
  wire [ADDR_WIDTH-1:0]	m_axi_araddr;		// From u03_cdc of axi_cdc_top.v
  wire [1:0]		m_axi_arburst;		// From u03_cdc of axi_cdc_top.v
  wire [3:0]		m_axi_arcache;		// From u03_cdc of axi_cdc_top.v
  wire [ID_WIDTH-1:0]	m_axi_arid;		// From u03_cdc of axi_cdc_top.v
  wire [7:0]		m_axi_arlen;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_arlock;		// From u03_cdc of axi_cdc_top.v
  wire [2:0]		m_axi_arprot;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_arready;		// From u04_ram of axi_ram.v
  wire [2:0]		m_axi_arsize;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_arvalid;		// From u03_cdc of axi_cdc_top.v
  wire [ADDR_WIDTH-1:0]	m_axi_awaddr;		// From u03_cdc of axi_cdc_top.v
  wire [1:0]		m_axi_awburst;		// From u03_cdc of axi_cdc_top.v
  wire [3:0]		m_axi_awcache;		// From u03_cdc of axi_cdc_top.v
  wire [ID_WIDTH-1:0]	m_axi_awid;		// From u03_cdc of axi_cdc_top.v
  wire [7:0]		m_axi_awlen;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_awlock;		// From u03_cdc of axi_cdc_top.v
  wire [2:0]		m_axi_awprot;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_awready;		// From u04_ram of axi_ram.v
  wire [2:0]		m_axi_awsize;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_awvalid;		// From u03_cdc of axi_cdc_top.v
  wire [ID_WIDTH-1:0]	m_axi_bid;		// From u04_ram of axi_ram.v
  wire			m_axi_bready;		// From u03_cdc of axi_cdc_top.v
  wire [1:0]		m_axi_bresp;		// From u04_ram of axi_ram.v
  wire			m_axi_bvalid;		// From u04_ram of axi_ram.v
  wire [DATA_WIDTH-1:0]	m_axi_rdata;		// From u04_ram of axi_ram.v
  wire [ID_WIDTH-1:0]	m_axi_rid;		// From u04_ram of axi_ram.v
  wire			m_axi_rlast;		// From u04_ram of axi_ram.v
  wire			m_axi_rready;		// From u03_cdc of axi_cdc_top.v
  wire [1:0]		m_axi_rresp;		// From u04_ram of axi_ram.v
  wire			m_axi_rvalid;		// From u04_ram of axi_ram.v
  wire [DATA_WIDTH-1:0]	m_axi_wdata;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_wlast;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_wready;		// From u04_ram of axi_ram.v
  wire [STRB_WIDTH-1:0]	m_axi_wstrb;		// From u03_cdc of axi_cdc_top.v
  wire			m_axi_wvalid;		// From u03_cdc of axi_cdc_top.v
  wire [AXI_ADDR_WIDTH-1:0] s00_axi_araddr;	// From u00_dma of axi_dma.v
  wire [1:0]		s00_axi_arburst;	// From u00_dma of axi_dma.v
  wire [3:0]		s00_axi_arcache;	// From u00_dma of axi_dma.v
  wire [AXI_ID_WIDTH-1:0] s00_axi_arid;		// From u00_dma of axi_dma.v
  wire [7:0]		s00_axi_arlen;		// From u00_dma of axi_dma.v
  wire			s00_axi_arlock;		// From u00_dma of axi_dma.v
  wire [2:0]		s00_axi_arprot;		// From u00_dma of axi_dma.v
  wire			s00_axi_arready;	// From u02_arbitor of axi_xbar_2x1.v
  wire [2:0]		s00_axi_arsize;		// From u00_dma of axi_dma.v
  wire			s00_axi_arvalid;	// From u00_dma of axi_dma.v
  wire [AXI_ADDR_WIDTH-1:0] s00_axi_awaddr;	// From u00_dma of axi_dma.v
  wire [1:0]		s00_axi_awburst;	// From u00_dma of axi_dma.v
  wire [3:0]		s00_axi_awcache;	// From u00_dma of axi_dma.v
  wire [AXI_ID_WIDTH-1:0] s00_axi_awid;		// From u00_dma of axi_dma.v
  wire [7:0]		s00_axi_awlen;		// From u00_dma of axi_dma.v
  wire			s00_axi_awlock;		// From u00_dma of axi_dma.v
  wire [2:0]		s00_axi_awprot;		// From u00_dma of axi_dma.v
  wire			s00_axi_awready;	// From u02_arbitor of axi_xbar_2x1.v
  wire [2:0]		s00_axi_awsize;		// From u00_dma of axi_dma.v
  wire			s00_axi_awvalid;	// From u00_dma of axi_dma.v
  wire [S_ID_WIDTH-1:0]	s00_axi_bid;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s00_axi_bready;		// From u00_dma of axi_dma.v
  wire [1:0]		s00_axi_bresp;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s00_axi_bvalid;		// From u02_arbitor of axi_xbar_2x1.v
  wire [DATA_WIDTH-1:0]	s00_axi_rdata;		// From u02_arbitor of axi_xbar_2x1.v
  wire [S_ID_WIDTH-1:0]	s00_axi_rid;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s00_axi_rlast;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s00_axi_rready;		// From u00_dma of axi_dma.v
  wire [1:0]		s00_axi_rresp;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s00_axi_rvalid;		// From u02_arbitor of axi_xbar_2x1.v
  wire [AXI_DATA_WIDTH-1:0] s00_axi_wdata;	// From u00_dma of axi_dma.v
  wire			s00_axi_wlast;		// From u00_dma of axi_dma.v
  wire			s00_axi_wready;		// From u02_arbitor of axi_xbar_2x1.v
  wire [AXI_STRB_WIDTH-1:0] s00_axi_wstrb;	// From u00_dma of axi_dma.v
  wire			s00_axi_wvalid;		// From u00_dma of axi_dma.v
  wire [AXI_ADDR_WIDTH-1:0] s01_axi_araddr;	// From u01_cdma of axi_cdma.v
  wire [1:0]		s01_axi_arburst;	// From u01_cdma of axi_cdma.v
  wire [3:0]		s01_axi_arcache;	// From u01_cdma of axi_cdma.v
  wire [AXI_ID_WIDTH-1:0] s01_axi_arid;		// From u01_cdma of axi_cdma.v
  wire [7:0]		s01_axi_arlen;		// From u01_cdma of axi_cdma.v
  wire			s01_axi_arlock;		// From u01_cdma of axi_cdma.v
  wire [2:0]		s01_axi_arprot;		// From u01_cdma of axi_cdma.v
  wire			s01_axi_arready;	// From u02_arbitor of axi_xbar_2x1.v
  wire [2:0]		s01_axi_arsize;		// From u01_cdma of axi_cdma.v
  wire			s01_axi_arvalid;	// From u01_cdma of axi_cdma.v
  wire [AXI_ADDR_WIDTH-1:0] s01_axi_awaddr;	// From u01_cdma of axi_cdma.v
  wire [1:0]		s01_axi_awburst;	// From u01_cdma of axi_cdma.v
  wire [3:0]		s01_axi_awcache;	// From u01_cdma of axi_cdma.v
  wire [AXI_ID_WIDTH-1:0] s01_axi_awid;		// From u01_cdma of axi_cdma.v
  wire [7:0]		s01_axi_awlen;		// From u01_cdma of axi_cdma.v
  wire			s01_axi_awlock;		// From u01_cdma of axi_cdma.v
  wire [2:0]		s01_axi_awprot;		// From u01_cdma of axi_cdma.v
  wire			s01_axi_awready;	// From u02_arbitor of axi_xbar_2x1.v
  wire [2:0]		s01_axi_awsize;		// From u01_cdma of axi_cdma.v
  wire			s01_axi_awvalid;	// From u01_cdma of axi_cdma.v
  wire [S_ID_WIDTH-1:0]	s01_axi_bid;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s01_axi_bready;		// From u01_cdma of axi_cdma.v
  wire [1:0]		s01_axi_bresp;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s01_axi_bvalid;		// From u02_arbitor of axi_xbar_2x1.v
  wire [DATA_WIDTH-1:0]	s01_axi_rdata;		// From u02_arbitor of axi_xbar_2x1.v
  wire [S_ID_WIDTH-1:0]	s01_axi_rid;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s01_axi_rlast;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s01_axi_rready;		// From u01_cdma of axi_cdma.v
  wire [1:0]		s01_axi_rresp;		// From u02_arbitor of axi_xbar_2x1.v
  wire			s01_axi_rvalid;		// From u02_arbitor of axi_xbar_2x1.v
  wire [AXI_DATA_WIDTH-1:0] s01_axi_wdata;	// From u01_cdma of axi_cdma.v
  wire			s01_axi_wlast;		// From u01_cdma of axi_cdma.v
  wire			s01_axi_wready;		// From u02_arbitor of axi_xbar_2x1.v
  wire [AXI_STRB_WIDTH-1:0] s01_axi_wstrb;	// From u01_cdma of axi_cdma.v
  wire			s01_axi_wvalid;		// From u01_cdma of axi_cdma.v
  // End of automatics

  /* axi_ram  AUTO_TEMPLATE (
	.clk  (clk1),
	.s_axi_\(.*\) 		(m_axi_\1[]),
 );
*/


/* axi_cdc_top AUTO_TEMPLATE (
	.rst0  (rst),
	.rst1  (rst),
	.clk0  (clk),
	.s_axi_\(.*\) 		(m00_axi_\1[]),
 );
*/


  /* axi_xbar_2x1  AUTO_TEMPLATE (
        .\(.*\)user (@"(if (equal vl-dir \\"input\\") (concat   \\"{\\"    vl-width   \\"{\\"   \\"1'b0\\"  \\"}\\"   \\"}\\"      ) \\"\\" )"),
        .\(.*\)qos (@"(if (equal vl-dir \\"input\\") (concat   \\"{\\"    vl-width   \\"{\\"   \\"1'b0\\"  \\"}\\"   \\"}\\"      ) \\"\\" )"),
        .\(.*\)region (@"(if (equal vl-dir \\"input\\") (concat   \\"{\\"    vl-width   \\"{\\"   \\"1'b0\\"  \\"}\\"   \\"}\\"      ) \\"\\" )"),
        .\(.*\)user (@"(if (equal vl-dir \\"output\\")  \\"\\"   \\"\\" )"),
        .\(.*\)qos (@"(if (equal vl-dir \\"output\\")  \\"\\"   \\"\\" )"),
        .\(.*\)region (@"(if (equal vl-dir \\"output\\")  \\"\\"   \\"\\" )"),
       ); */

  /* axi_dma  AUTO_TEMPLATE (
	.m_axi_\(.*\) 		(s00_axi_\1[]),
 );
*/

  /* axi_cdma  AUTO_TEMPLATE (
	.m_axi_\(.*\) 		(s01_axi_\1[]),
 );
*/

  ///* axi_xbar_2x1  AUTO_TEMPLATE (
  //  .m00\(.*\)user(@"(if (equal vl-dir \\"input\\") \\"'d0\\" \\"\\")"),
  //  .m00\(.*\)user(),
  // );
  //*/

  //  /* axi_xbar_2x1  AUTO_TEMPLATE (
  //  //output
  //  .m00_axi_wuser     ()  , 
  //  .m00_axi_awuser    ()  ,
  //  .m00_axi_awregion  ()  , 
  //  .m00_axi_awqos     ()  , 
  //  .m00_axi_aruser    ()  , 
  //  .m00_axi_arregion  ()  ,
  //  .m00_axi_arqos     ()  ,
  //
  //  //input
  //  .m00_axi_ruser ('d0)       ,
  //  .m00_axi_buser ('d0)       ,
  //
  // );
  //*/
  //

axi_dma#(
      /*AUTOINSTPARAM*/
	 // Parameters
	 .AXI_DATA_WIDTH		(AXI_DATA_WIDTH),
	 .AXI_ADDR_WIDTH		(AXI_ADDR_WIDTH),
	 .AXI_STRB_WIDTH		(AXI_STRB_WIDTH),
	 .AXI_ID_WIDTH			(AXI_ID_WIDTH),
	 .AXI_MAX_BURST_LEN		(AXI_MAX_BURST_LEN),
	 .AXIS_DATA_WIDTH		(AXIS_DATA_WIDTH),
	 .AXIS_KEEP_ENABLE		(AXIS_KEEP_ENABLE),
	 .AXIS_KEEP_WIDTH		(AXIS_KEEP_WIDTH),
	 .AXIS_LAST_ENABLE		(AXIS_LAST_ENABLE),
	 .AXIS_ID_ENABLE		(AXIS_ID_ENABLE),
	 .AXIS_ID_WIDTH			(AXIS_ID_WIDTH),
	 .AXIS_DEST_ENABLE		(AXIS_DEST_ENABLE),
	 .AXIS_DEST_WIDTH		(AXIS_DEST_WIDTH),
	 .AXIS_USER_ENABLE		(AXIS_USER_ENABLE),
	 .AXIS_USER_WIDTH		(AXIS_USER_WIDTH),
	 .LEN_WIDTH			(LEN_WIDTH),
	 .TAG_WIDTH			(TAG_WIDTH),
	 .ENABLE_SG			(ENABLE_SG),
	 .ENABLE_UNALIGNED		(ENABLE_UNALIGNED))u00_dma(
            /*autoinst*/
								   // Outputs
								   .s_axis_read_desc_ready(s_axis_read_desc_ready),
								   .m_axis_read_desc_status_tag(m_axis_read_desc_status_tag[TAG_WIDTH-1:0]),
								   .m_axis_read_desc_status_error(m_axis_read_desc_status_error[3:0]),
								   .m_axis_read_desc_status_valid(m_axis_read_desc_status_valid),
								   .m_axis_read_data_tdata(m_axis_read_data_tdata[AXIS_DATA_WIDTH-1:0]),
								   .m_axis_read_data_tkeep(m_axis_read_data_tkeep[AXIS_KEEP_WIDTH-1:0]),
								   .m_axis_read_data_tvalid(m_axis_read_data_tvalid),
								   .m_axis_read_data_tlast(m_axis_read_data_tlast),
								   .m_axis_read_data_tid(m_axis_read_data_tid[AXIS_ID_WIDTH-1:0]),
								   .m_axis_read_data_tdest(m_axis_read_data_tdest[AXIS_DEST_WIDTH-1:0]),
								   .m_axis_read_data_tuser(m_axis_read_data_tuser[AXIS_USER_WIDTH-1:0]),
								   .s_axis_write_desc_ready(s_axis_write_desc_ready),
								   .m_axis_write_desc_status_len(m_axis_write_desc_status_len[LEN_WIDTH-1:0]),
								   .m_axis_write_desc_status_tag(m_axis_write_desc_status_tag[TAG_WIDTH-1:0]),
								   .m_axis_write_desc_status_id(m_axis_write_desc_status_id[AXIS_ID_WIDTH-1:0]),
								   .m_axis_write_desc_status_dest(m_axis_write_desc_status_dest[AXIS_DEST_WIDTH-1:0]),
								   .m_axis_write_desc_status_user(m_axis_write_desc_status_user[AXIS_USER_WIDTH-1:0]),
								   .m_axis_write_desc_status_error(m_axis_write_desc_status_error[3:0]),
								   .m_axis_write_desc_status_valid(m_axis_write_desc_status_valid),
								   .s_axis_write_data_tready(s_axis_write_data_tready),
								   .m_axi_awid		(s00_axi_awid[AXI_ID_WIDTH-1:0]), // Templated
								   .m_axi_awaddr	(s00_axi_awaddr[AXI_ADDR_WIDTH-1:0]), // Templated
								   .m_axi_awlen		(s00_axi_awlen[7:0]), // Templated
								   .m_axi_awsize	(s00_axi_awsize[2:0]), // Templated
								   .m_axi_awburst	(s00_axi_awburst[1:0]), // Templated
								   .m_axi_awlock	(s00_axi_awlock), // Templated
								   .m_axi_awcache	(s00_axi_awcache[3:0]), // Templated
								   .m_axi_awprot	(s00_axi_awprot[2:0]), // Templated
								   .m_axi_awvalid	(s00_axi_awvalid), // Templated
								   .m_axi_wdata		(s00_axi_wdata[AXI_DATA_WIDTH-1:0]), // Templated
								   .m_axi_wstrb		(s00_axi_wstrb[AXI_STRB_WIDTH-1:0]), // Templated
								   .m_axi_wlast		(s00_axi_wlast), // Templated
								   .m_axi_wvalid	(s00_axi_wvalid), // Templated
								   .m_axi_bready	(s00_axi_bready), // Templated
								   .m_axi_arid		(s00_axi_arid[AXI_ID_WIDTH-1:0]), // Templated
								   .m_axi_araddr	(s00_axi_araddr[AXI_ADDR_WIDTH-1:0]), // Templated
								   .m_axi_arlen		(s00_axi_arlen[7:0]), // Templated
								   .m_axi_arsize	(s00_axi_arsize[2:0]), // Templated
								   .m_axi_arburst	(s00_axi_arburst[1:0]), // Templated
								   .m_axi_arlock	(s00_axi_arlock), // Templated
								   .m_axi_arcache	(s00_axi_arcache[3:0]), // Templated
								   .m_axi_arprot	(s00_axi_arprot[2:0]), // Templated
								   .m_axi_arvalid	(s00_axi_arvalid), // Templated
								   .m_axi_rready	(s00_axi_rready), // Templated
								   // Inputs
								   .clk			(clk),
								   .rst			(rst),
								   .s_axis_read_desc_addr(s_axis_read_desc_addr[AXI_ADDR_WIDTH-1:0]),
								   .s_axis_read_desc_len(s_axis_read_desc_len[LEN_WIDTH-1:0]),
								   .s_axis_read_desc_tag(s_axis_read_desc_tag[TAG_WIDTH-1:0]),
								   .s_axis_read_desc_id	(s_axis_read_desc_id[AXIS_ID_WIDTH-1:0]),
								   .s_axis_read_desc_dest(s_axis_read_desc_dest[AXIS_DEST_WIDTH-1:0]),
								   .s_axis_read_desc_user(s_axis_read_desc_user[AXIS_USER_WIDTH-1:0]),
								   .s_axis_read_desc_valid(s_axis_read_desc_valid),
								   .m_axis_read_data_tready(m_axis_read_data_tready),
								   .s_axis_write_desc_addr(s_axis_write_desc_addr[AXI_ADDR_WIDTH-1:0]),
								   .s_axis_write_desc_len(s_axis_write_desc_len[LEN_WIDTH-1:0]),
								   .s_axis_write_desc_tag(s_axis_write_desc_tag[TAG_WIDTH-1:0]),
								   .s_axis_write_desc_valid(s_axis_write_desc_valid),
								   .s_axis_write_data_tdata(s_axis_write_data_tdata[AXIS_DATA_WIDTH-1:0]),
								   .s_axis_write_data_tkeep(s_axis_write_data_tkeep[AXIS_KEEP_WIDTH-1:0]),
								   .s_axis_write_data_tvalid(s_axis_write_data_tvalid),
								   .s_axis_write_data_tlast(s_axis_write_data_tlast),
								   .s_axis_write_data_tid(s_axis_write_data_tid[AXIS_ID_WIDTH-1:0]),
								   .s_axis_write_data_tdest(s_axis_write_data_tdest[AXIS_DEST_WIDTH-1:0]),
								   .s_axis_write_data_tuser(s_axis_write_data_tuser[AXIS_USER_WIDTH-1:0]),
								   .m_axi_awready	(s00_axi_awready), // Templated
								   .m_axi_wready	(s00_axi_wready), // Templated
								   .m_axi_bid		(s00_axi_bid[AXI_ID_WIDTH-1:0]), // Templated
								   .m_axi_bresp		(s00_axi_bresp[1:0]), // Templated
								   .m_axi_bvalid	(s00_axi_bvalid), // Templated
								   .m_axi_arready	(s00_axi_arready), // Templated
								   .m_axi_rid		(s00_axi_rid[AXI_ID_WIDTH-1:0]), // Templated
								   .m_axi_rdata		(s00_axi_rdata[AXI_DATA_WIDTH-1:0]), // Templated
								   .m_axi_rresp		(s00_axi_rresp[1:0]), // Templated
								   .m_axi_rlast		(s00_axi_rlast), // Templated
								   .m_axi_rvalid	(s00_axi_rvalid), // Templated
								   .read_enable		(read_enable),
								   .write_enable	(write_enable),
								   .write_abort		(write_abort));


axi_cdma#(
      /*AUTOINSTPARAM*/
	  // Parameters
	  .AXI_DATA_WIDTH		(AXI_DATA_WIDTH),
	  .AXI_ADDR_WIDTH		(AXI_ADDR_WIDTH),
	  .AXI_STRB_WIDTH		(AXI_STRB_WIDTH),
	  .AXI_ID_WIDTH			(AXI_ID_WIDTH),
	  .AXI_MAX_BURST_LEN		(AXI_MAX_BURST_LEN),
	  .LEN_WIDTH			(LEN_WIDTH),
	  .TAG_WIDTH			(TAG_WIDTH),
	  .ENABLE_UNALIGNED		(ENABLE_UNALIGNED),
	  .AXI_WORD_WIDTH		(AXI_WORD_WIDTH),
	  .AXI_WORD_SIZE		(AXI_WORD_SIZE),
	  .AXI_BURST_SIZE		(AXI_BURST_SIZE),
	  .AXI_MAX_BURST_SIZE		(AXI_MAX_BURST_SIZE),
	  .OFFSET_WIDTH			(OFFSET_WIDTH),
	  .OFFSET_MASK			(OFFSET_MASK),
	  .ADDR_MASK			(ADDR_MASK),
	  .CYCLE_COUNT_WIDTH		(CYCLE_COUNT_WIDTH),
	  .STATUS_FIFO_ADDR_WIDTH	(STATUS_FIFO_ADDR_WIDTH),
	  .OUTPUT_FIFO_ADDR_WIDTH	(OUTPUT_FIFO_ADDR_WIDTH))u01_cdma(
            /*autoinst*/
									  // Outputs
									  .s_axis_desc_ready	(s_axis_desc_ready),
									  .m_axis_desc_status_tag(m_axis_desc_status_tag[TAG_WIDTH-1:0]),
									  .m_axis_desc_status_error(m_axis_desc_status_error[3:0]),
									  .m_axis_desc_status_valid(m_axis_desc_status_valid),
									  .m_axi_awid		(s01_axi_awid[AXI_ID_WIDTH-1:0]), // Templated
									  .m_axi_awaddr		(s01_axi_awaddr[AXI_ADDR_WIDTH-1:0]), // Templated
									  .m_axi_awlen		(s01_axi_awlen[7:0]), // Templated
									  .m_axi_awsize		(s01_axi_awsize[2:0]), // Templated
									  .m_axi_awburst	(s01_axi_awburst[1:0]), // Templated
									  .m_axi_awlock		(s01_axi_awlock), // Templated
									  .m_axi_awcache	(s01_axi_awcache[3:0]), // Templated
									  .m_axi_awprot		(s01_axi_awprot[2:0]), // Templated
									  .m_axi_awvalid	(s01_axi_awvalid), // Templated
									  .m_axi_wdata		(s01_axi_wdata[AXI_DATA_WIDTH-1:0]), // Templated
									  .m_axi_wstrb		(s01_axi_wstrb[AXI_STRB_WIDTH-1:0]), // Templated
									  .m_axi_wlast		(s01_axi_wlast), // Templated
									  .m_axi_wvalid		(s01_axi_wvalid), // Templated
									  .m_axi_bready		(s01_axi_bready), // Templated
									  .m_axi_arid		(s01_axi_arid[AXI_ID_WIDTH-1:0]), // Templated
									  .m_axi_araddr		(s01_axi_araddr[AXI_ADDR_WIDTH-1:0]), // Templated
									  .m_axi_arlen		(s01_axi_arlen[7:0]), // Templated
									  .m_axi_arsize		(s01_axi_arsize[2:0]), // Templated
									  .m_axi_arburst	(s01_axi_arburst[1:0]), // Templated
									  .m_axi_arlock		(s01_axi_arlock), // Templated
									  .m_axi_arcache	(s01_axi_arcache[3:0]), // Templated
									  .m_axi_arprot		(s01_axi_arprot[2:0]), // Templated
									  .m_axi_arvalid	(s01_axi_arvalid), // Templated
									  .m_axi_rready		(s01_axi_rready), // Templated
									  // Inputs
									  .clk			(clk),
									  .rst			(rst),
									  .s_axis_desc_read_addr(s_axis_desc_read_addr[AXI_ADDR_WIDTH-1:0]),
									  .s_axis_desc_write_addr(s_axis_desc_write_addr[AXI_ADDR_WIDTH-1:0]),
									  .s_axis_desc_len	(s_axis_desc_len[LEN_WIDTH-1:0]),
									  .s_axis_desc_tag	(s_axis_desc_tag[TAG_WIDTH-1:0]),
									  .s_axis_desc_valid	(s_axis_desc_valid),
									  .m_axi_awready	(s01_axi_awready), // Templated
									  .m_axi_wready		(s01_axi_wready), // Templated
									  .m_axi_bid		(s01_axi_bid[AXI_ID_WIDTH-1:0]), // Templated
									  .m_axi_bresp		(s01_axi_bresp[1:0]), // Templated
									  .m_axi_bvalid		(s01_axi_bvalid), // Templated
									  .m_axi_arready	(s01_axi_arready), // Templated
									  .m_axi_rid		(s01_axi_rid[AXI_ID_WIDTH-1:0]), // Templated
									  .m_axi_rdata		(s01_axi_rdata[AXI_DATA_WIDTH-1:0]), // Templated
									  .m_axi_rresp		(s01_axi_rresp[1:0]), // Templated
									  .m_axi_rlast		(s01_axi_rlast), // Templated
									  .m_axi_rvalid		(s01_axi_rvalid), // Templated
									  .enable		(enable));



  axi_xbar_2x1 #(
      /*AUTOINSTPARAM*/
		 // Parameters
		 .DATA_WIDTH		(DATA_WIDTH),
		 .ADDR_WIDTH		(ADDR_WIDTH),
		 .STRB_WIDTH		(STRB_WIDTH),
		 .S_ID_WIDTH		(S_ID_WIDTH),
		 .M_ID_WIDTH		(M_ID_WIDTH),
		 .AWUSER_ENABLE		(AWUSER_ENABLE),
		 .AWUSER_WIDTH		(AWUSER_WIDTH),
		 .WUSER_ENABLE		(WUSER_ENABLE),
		 .WUSER_WIDTH		(WUSER_WIDTH),
		 .BUSER_ENABLE		(BUSER_ENABLE),
		 .BUSER_WIDTH		(BUSER_WIDTH),
		 .ARUSER_ENABLE		(ARUSER_ENABLE),
		 .ARUSER_WIDTH		(ARUSER_WIDTH),
		 .RUSER_ENABLE		(RUSER_ENABLE),
		 .RUSER_WIDTH		(RUSER_WIDTH),
		 .S00_THREADS		(S00_THREADS),
		 .S00_ACCEPT		(S00_ACCEPT),
		 .S01_THREADS		(S01_THREADS),
		 .S01_ACCEPT		(S01_ACCEPT),
		 .M_REGIONS		(M_REGIONS),
		 .M00_BASE_ADDR		(M00_BASE_ADDR),
		 .M00_ADDR_WIDTH	(M00_ADDR_WIDTH),
		 .M00_CONNECT_READ	(M00_CONNECT_READ),
		 .M00_CONNECT_WRITE	(M00_CONNECT_WRITE),
		 .M00_ISSUE		(M00_ISSUE),
		 .M00_SECURE		(M00_SECURE),
		 .S00_AW_REG_TYPE	(S00_AW_REG_TYPE),
		 .S00_W_REG_TYPE	(S00_W_REG_TYPE),
		 .S00_B_REG_TYPE	(S00_B_REG_TYPE),
		 .S00_AR_REG_TYPE	(S00_AR_REG_TYPE),
		 .S00_R_REG_TYPE	(S00_R_REG_TYPE),
		 .S01_AW_REG_TYPE	(S01_AW_REG_TYPE),
		 .S01_W_REG_TYPE	(S01_W_REG_TYPE),
		 .S01_B_REG_TYPE	(S01_B_REG_TYPE),
		 .S01_AR_REG_TYPE	(S01_AR_REG_TYPE),
		 .S01_R_REG_TYPE	(S01_R_REG_TYPE),
		 .M00_AW_REG_TYPE	(M00_AW_REG_TYPE),
		 .M00_W_REG_TYPE	(M00_W_REG_TYPE),
		 .M00_B_REG_TYPE	(M00_B_REG_TYPE),
		 .M00_AR_REG_TYPE	(M00_AR_REG_TYPE),
		 .M00_R_REG_TYPE	(M00_R_REG_TYPE)) u02_arbitor (  /*autoinst*/
								       // Outputs
								       .s00_axi_awready	(s00_axi_awready),
								       .s00_axi_wready	(s00_axi_wready),
								       .s00_axi_bid	(s00_axi_bid[S_ID_WIDTH-1:0]),
								       .s00_axi_bresp	(s00_axi_bresp[1:0]),
								       .s00_axi_buser	(),		 // Templated
								       .s00_axi_bvalid	(s00_axi_bvalid),
								       .s00_axi_arready	(s00_axi_arready),
								       .s00_axi_rid	(s00_axi_rid[S_ID_WIDTH-1:0]),
								       .s00_axi_rdata	(s00_axi_rdata[DATA_WIDTH-1:0]),
								       .s00_axi_rresp	(s00_axi_rresp[1:0]),
								       .s00_axi_rlast	(s00_axi_rlast),
								       .s00_axi_ruser	(),		 // Templated
								       .s00_axi_rvalid	(s00_axi_rvalid),
								       .s01_axi_awready	(s01_axi_awready),
								       .s01_axi_wready	(s01_axi_wready),
								       .s01_axi_bid	(s01_axi_bid[S_ID_WIDTH-1:0]),
								       .s01_axi_bresp	(s01_axi_bresp[1:0]),
								       .s01_axi_buser	(),		 // Templated
								       .s01_axi_bvalid	(s01_axi_bvalid),
								       .s01_axi_arready	(s01_axi_arready),
								       .s01_axi_rid	(s01_axi_rid[S_ID_WIDTH-1:0]),
								       .s01_axi_rdata	(s01_axi_rdata[DATA_WIDTH-1:0]),
								       .s01_axi_rresp	(s01_axi_rresp[1:0]),
								       .s01_axi_rlast	(s01_axi_rlast),
								       .s01_axi_ruser	(),		 // Templated
								       .s01_axi_rvalid	(s01_axi_rvalid),
								       .m00_axi_awid	(m00_axi_awid[M_ID_WIDTH-1:0]),
								       .m00_axi_awaddr	(m00_axi_awaddr[ADDR_WIDTH-1:0]),
								       .m00_axi_awlen	(m00_axi_awlen[7:0]),
								       .m00_axi_awsize	(m00_axi_awsize[2:0]),
								       .m00_axi_awburst	(m00_axi_awburst[1:0]),
								       .m00_axi_awlock	(m00_axi_awlock),
								       .m00_axi_awcache	(m00_axi_awcache[3:0]),
								       .m00_axi_awprot	(m00_axi_awprot[2:0]),
								       .m00_axi_awqos	(),		 // Templated
								       .m00_axi_awregion(),		 // Templated
								       .m00_axi_awuser	(),		 // Templated
								       .m00_axi_awvalid	(m00_axi_awvalid),
								       .m00_axi_wdata	(m00_axi_wdata[DATA_WIDTH-1:0]),
								       .m00_axi_wstrb	(m00_axi_wstrb[STRB_WIDTH-1:0]),
								       .m00_axi_wlast	(m00_axi_wlast),
								       .m00_axi_wuser	(),		 // Templated
								       .m00_axi_wvalid	(m00_axi_wvalid),
								       .m00_axi_bready	(m00_axi_bready),
								       .m00_axi_arid	(m00_axi_arid[M_ID_WIDTH-1:0]),
								       .m00_axi_araddr	(m00_axi_araddr[ADDR_WIDTH-1:0]),
								       .m00_axi_arlen	(m00_axi_arlen[7:0]),
								       .m00_axi_arsize	(m00_axi_arsize[2:0]),
								       .m00_axi_arburst	(m00_axi_arburst[1:0]),
								       .m00_axi_arlock	(m00_axi_arlock),
								       .m00_axi_arcache	(m00_axi_arcache[3:0]),
								       .m00_axi_arprot	(m00_axi_arprot[2:0]),
								       .m00_axi_arqos	(),		 // Templated
								       .m00_axi_arregion(),		 // Templated
								       .m00_axi_aruser	(),		 // Templated
								       .m00_axi_arvalid	(m00_axi_arvalid),
								       .m00_axi_rready	(m00_axi_rready),
								       // Inputs
								       .clk		(clk),
								       .rst		(rst),
								       .s00_axi_awid	(s00_axi_awid[S_ID_WIDTH-1:0]),
								       .s00_axi_awaddr	(s00_axi_awaddr[ADDR_WIDTH-1:0]),
								       .s00_axi_awlen	(s00_axi_awlen[7:0]),
								       .s00_axi_awsize	(s00_axi_awsize[2:0]),
								       .s00_axi_awburst	(s00_axi_awburst[1:0]),
								       .s00_axi_awlock	(s00_axi_awlock),
								       .s00_axi_awcache	(s00_axi_awcache[3:0]),
								       .s00_axi_awprot	(s00_axi_awprot[2:0]),
								       .s00_axi_awqos	({4{1'b0}}),	 // Templated
								       .s00_axi_awuser	({AWUSER_WIDTH{1'b0}}), // Templated
								       .s00_axi_awvalid	(s00_axi_awvalid),
								       .s00_axi_wdata	(s00_axi_wdata[DATA_WIDTH-1:0]),
								       .s00_axi_wstrb	(s00_axi_wstrb[STRB_WIDTH-1:0]),
								       .s00_axi_wlast	(s00_axi_wlast),
								       .s00_axi_wuser	({WUSER_WIDTH{1'b0}}), // Templated
								       .s00_axi_wvalid	(s00_axi_wvalid),
								       .s00_axi_bready	(s00_axi_bready),
								       .s00_axi_arid	(s00_axi_arid[S_ID_WIDTH-1:0]),
								       .s00_axi_araddr	(s00_axi_araddr[ADDR_WIDTH-1:0]),
								       .s00_axi_arlen	(s00_axi_arlen[7:0]),
								       .s00_axi_arsize	(s00_axi_arsize[2:0]),
								       .s00_axi_arburst	(s00_axi_arburst[1:0]),
								       .s00_axi_arlock	(s00_axi_arlock),
								       .s00_axi_arcache	(s00_axi_arcache[3:0]),
								       .s00_axi_arprot	(s00_axi_arprot[2:0]),
								       .s00_axi_arqos	({4{1'b0}}),	 // Templated
								       .s00_axi_aruser	({ARUSER_WIDTH{1'b0}}), // Templated
								       .s00_axi_arvalid	(s00_axi_arvalid),
								       .s00_axi_rready	(s00_axi_rready),
								       .s01_axi_awid	(s01_axi_awid[S_ID_WIDTH-1:0]),
								       .s01_axi_awaddr	(s01_axi_awaddr[ADDR_WIDTH-1:0]),
								       .s01_axi_awlen	(s01_axi_awlen[7:0]),
								       .s01_axi_awsize	(s01_axi_awsize[2:0]),
								       .s01_axi_awburst	(s01_axi_awburst[1:0]),
								       .s01_axi_awlock	(s01_axi_awlock),
								       .s01_axi_awcache	(s01_axi_awcache[3:0]),
								       .s01_axi_awprot	(s01_axi_awprot[2:0]),
								       .s01_axi_awqos	({4{1'b0}}),	 // Templated
								       .s01_axi_awuser	({AWUSER_WIDTH{1'b0}}), // Templated
								       .s01_axi_awvalid	(s01_axi_awvalid),
								       .s01_axi_wdata	(s01_axi_wdata[DATA_WIDTH-1:0]),
								       .s01_axi_wstrb	(s01_axi_wstrb[STRB_WIDTH-1:0]),
								       .s01_axi_wlast	(s01_axi_wlast),
								       .s01_axi_wuser	({WUSER_WIDTH{1'b0}}), // Templated
								       .s01_axi_wvalid	(s01_axi_wvalid),
								       .s01_axi_bready	(s01_axi_bready),
								       .s01_axi_arid	(s01_axi_arid[S_ID_WIDTH-1:0]),
								       .s01_axi_araddr	(s01_axi_araddr[ADDR_WIDTH-1:0]),
								       .s01_axi_arlen	(s01_axi_arlen[7:0]),
								       .s01_axi_arsize	(s01_axi_arsize[2:0]),
								       .s01_axi_arburst	(s01_axi_arburst[1:0]),
								       .s01_axi_arlock	(s01_axi_arlock),
								       .s01_axi_arcache	(s01_axi_arcache[3:0]),
								       .s01_axi_arprot	(s01_axi_arprot[2:0]),
								       .s01_axi_arqos	({4{1'b0}}),	 // Templated
								       .s01_axi_aruser	({ARUSER_WIDTH{1'b0}}), // Templated
								       .s01_axi_arvalid	(s01_axi_arvalid),
								       .s01_axi_rready	(s01_axi_rready),
								       .m00_axi_awready	(m00_axi_awready),
								       .m00_axi_wready	(m00_axi_wready),
								       .m00_axi_bid	(m00_axi_bid[M_ID_WIDTH-1:0]),
								       .m00_axi_bresp	(m00_axi_bresp[1:0]),
								       .m00_axi_buser	({BUSER_WIDTH{1'b0}}), // Templated
								       .m00_axi_bvalid	(m00_axi_bvalid),
								       .m00_axi_arready	(m00_axi_arready),
								       .m00_axi_rid	(m00_axi_rid[M_ID_WIDTH-1:0]),
								       .m00_axi_rdata	(m00_axi_rdata[DATA_WIDTH-1:0]),
								       .m00_axi_rresp	(m00_axi_rresp[1:0]),
								       .m00_axi_rlast	(m00_axi_rlast),
								       .m00_axi_ruser	({RUSER_WIDTH{1'b0}}), // Templated
								       .m00_axi_rvalid	(m00_axi_rvalid));





  axi_cdc_top #(
      /*AUTOINSTPARAM*/
		// Parameters
		.DATA_WIDTH		(DATA_WIDTH),
		.ADDR_WIDTH		(ADDR_WIDTH),
		.STRB_WIDTH		(STRB_WIDTH),
		.ID_WIDTH		(ID_WIDTH),
		.PIPELINE_OUTPUT	(PIPELINE_OUTPUT)) u03_cdc  (  /*autoinst*/
								     // Outputs
								     .s_axi_awready	(m00_axi_awready), // Templated
								     .s_axi_wready	(m00_axi_wready), // Templated
								     .s_axi_bid		(m00_axi_bid[ID_WIDTH-1:0]), // Templated
								     .s_axi_bresp	(m00_axi_bresp[1:0]), // Templated
								     .s_axi_bvalid	(m00_axi_bvalid), // Templated
								     .s_axi_arready	(m00_axi_arready), // Templated
								     .s_axi_rid		(m00_axi_rid[ID_WIDTH-1:0]), // Templated
								     .s_axi_rdata	(m00_axi_rdata[DATA_WIDTH-1:0]), // Templated
								     .s_axi_rresp	(m00_axi_rresp[1:0]), // Templated
								     .s_axi_rlast	(m00_axi_rlast), // Templated
								     .s_axi_rvalid	(m00_axi_rvalid), // Templated
								     .m_axi_awid	(m_axi_awid[ID_WIDTH-1:0]),
								     .m_axi_awaddr	(m_axi_awaddr[ADDR_WIDTH-1:0]),
								     .m_axi_awlen	(m_axi_awlen[7:0]),
								     .m_axi_awsize	(m_axi_awsize[2:0]),
								     .m_axi_awburst	(m_axi_awburst[1:0]),
								     .m_axi_awlock	(m_axi_awlock),
								     .m_axi_awcache	(m_axi_awcache[3:0]),
								     .m_axi_awprot	(m_axi_awprot[2:0]),
								     .m_axi_awvalid	(m_axi_awvalid),
								     .m_axi_wdata	(m_axi_wdata[DATA_WIDTH-1:0]),
								     .m_axi_wstrb	(m_axi_wstrb[STRB_WIDTH-1:0]),
								     .m_axi_wlast	(m_axi_wlast),
								     .m_axi_wvalid	(m_axi_wvalid),
								     .m_axi_bready	(m_axi_bready),
								     .m_axi_arid	(m_axi_arid[ID_WIDTH-1:0]),
								     .m_axi_araddr	(m_axi_araddr[ADDR_WIDTH-1:0]),
								     .m_axi_arlen	(m_axi_arlen[7:0]),
								     .m_axi_arsize	(m_axi_arsize[2:0]),
								     .m_axi_arburst	(m_axi_arburst[1:0]),
								     .m_axi_arlock	(m_axi_arlock),
								     .m_axi_arcache	(m_axi_arcache[3:0]),
								     .m_axi_arprot	(m_axi_arprot[2:0]),
								     .m_axi_arvalid	(m_axi_arvalid),
								     .m_axi_rready	(m_axi_rready),
								     // Inputs
								     .clk0		(clk),		 // Templated
								     .rst0		(rst),		 // Templated
								     .clk1		(clk1),
								     .rst1		(rst),		 // Templated
								     .s_axi_awid	(m00_axi_awid[ID_WIDTH-1:0]), // Templated
								     .s_axi_awaddr	(m00_axi_awaddr[ADDR_WIDTH-1:0]), // Templated
								     .s_axi_awlen	(m00_axi_awlen[7:0]), // Templated
								     .s_axi_awsize	(m00_axi_awsize[2:0]), // Templated
								     .s_axi_awburst	(m00_axi_awburst[1:0]), // Templated
								     .s_axi_awlock	(m00_axi_awlock), // Templated
								     .s_axi_awcache	(m00_axi_awcache[3:0]), // Templated
								     .s_axi_awprot	(m00_axi_awprot[2:0]), // Templated
								     .s_axi_awvalid	(m00_axi_awvalid), // Templated
								     .s_axi_wdata	(m00_axi_wdata[DATA_WIDTH-1:0]), // Templated
								     .s_axi_wstrb	(m00_axi_wstrb[STRB_WIDTH-1:0]), // Templated
								     .s_axi_wlast	(m00_axi_wlast), // Templated
								     .s_axi_wvalid	(m00_axi_wvalid), // Templated
								     .s_axi_bready	(m00_axi_bready), // Templated
								     .s_axi_arid	(m00_axi_arid[ID_WIDTH-1:0]), // Templated
								     .s_axi_araddr	(m00_axi_araddr[ADDR_WIDTH-1:0]), // Templated
								     .s_axi_arlen	(m00_axi_arlen[7:0]), // Templated
								     .s_axi_arsize	(m00_axi_arsize[2:0]), // Templated
								     .s_axi_arburst	(m00_axi_arburst[1:0]), // Templated
								     .s_axi_arlock	(m00_axi_arlock), // Templated
								     .s_axi_arcache	(m00_axi_arcache[3:0]), // Templated
								     .s_axi_arprot	(m00_axi_arprot[2:0]), // Templated
								     .s_axi_arvalid	(m00_axi_arvalid), // Templated
								     .s_axi_rready	(m00_axi_rready), // Templated
								     .m_axi_awready	(m_axi_awready),
								     .m_axi_wready	(m_axi_wready),
								     .m_axi_bid		(m_axi_bid[ID_WIDTH-1:0]),
								     .m_axi_bresp	(m_axi_bresp[1:0]),
								     .m_axi_bvalid	(m_axi_bvalid),
								     .m_axi_arready	(m_axi_arready),
								     .m_axi_rid		(m_axi_rid[ID_WIDTH-1:0]),
								     .m_axi_rdata	(m_axi_rdata[DATA_WIDTH-1:0]),
								     .m_axi_rresp	(m_axi_rresp[1:0]),
								     .m_axi_rlast	(m_axi_rlast),
								     .m_axi_rvalid	(m_axi_rvalid));




  axi_ram #(
      /*AUTOINSTPARAM*/
	    // Parameters
	    .DATA_WIDTH			(DATA_WIDTH),
	    .ADDR_WIDTH			(ADDR_WIDTH),
	    .STRB_WIDTH			(STRB_WIDTH),
	    .ID_WIDTH			(ID_WIDTH),
	    .PIPELINE_OUTPUT		(PIPELINE_OUTPUT),
	    .VALID_ADDR_WIDTH		(VALID_ADDR_WIDTH),
	    .WORD_WIDTH			(WORD_WIDTH),
	    .WORD_SIZE			(WORD_SIZE)) u04_ram (  /*autoinst*/
							      // Outputs
							      .s_axi_awready	(m_axi_awready), // Templated
							      .s_axi_wready	(m_axi_wready),	 // Templated
							      .s_axi_bid	(m_axi_bid[ID_WIDTH-1:0]), // Templated
							      .s_axi_bresp	(m_axi_bresp[1:0]), // Templated
							      .s_axi_bvalid	(m_axi_bvalid),	 // Templated
							      .s_axi_arready	(m_axi_arready), // Templated
							      .s_axi_rid	(m_axi_rid[ID_WIDTH-1:0]), // Templated
							      .s_axi_rdata	(m_axi_rdata[DATA_WIDTH-1:0]), // Templated
							      .s_axi_rresp	(m_axi_rresp[1:0]), // Templated
							      .s_axi_rlast	(m_axi_rlast),	 // Templated
							      .s_axi_rvalid	(m_axi_rvalid),	 // Templated
							      // Inputs
							      .clk		(clk1),		 // Templated
							      .rst		(rst),
							      .s_axi_awid	(m_axi_awid[ID_WIDTH-1:0]), // Templated
							      .s_axi_awaddr	(m_axi_awaddr[ADDR_WIDTH-1:0]), // Templated
							      .s_axi_awlen	(m_axi_awlen[7:0]), // Templated
							      .s_axi_awsize	(m_axi_awsize[2:0]), // Templated
							      .s_axi_awburst	(m_axi_awburst[1:0]), // Templated
							      .s_axi_awlock	(m_axi_awlock),	 // Templated
							      .s_axi_awcache	(m_axi_awcache[3:0]), // Templated
							      .s_axi_awprot	(m_axi_awprot[2:0]), // Templated
							      .s_axi_awvalid	(m_axi_awvalid), // Templated
							      .s_axi_wdata	(m_axi_wdata[DATA_WIDTH-1:0]), // Templated
							      .s_axi_wstrb	(m_axi_wstrb[STRB_WIDTH-1:0]), // Templated
							      .s_axi_wlast	(m_axi_wlast),	 // Templated
							      .s_axi_wvalid	(m_axi_wvalid),	 // Templated
							      .s_axi_bready	(m_axi_bready),	 // Templated
							      .s_axi_arid	(m_axi_arid[ID_WIDTH-1:0]), // Templated
							      .s_axi_araddr	(m_axi_araddr[ADDR_WIDTH-1:0]), // Templated
							      .s_axi_arlen	(m_axi_arlen[7:0]), // Templated
							      .s_axi_arsize	(m_axi_arsize[2:0]), // Templated
							      .s_axi_arburst	(m_axi_arburst[1:0]), // Templated
							      .s_axi_arlock	(m_axi_arlock),	 // Templated
							      .s_axi_arcache	(m_axi_arcache[3:0]), // Templated
							      .s_axi_arprot	(m_axi_arprot[2:0]), // Templated
							      .s_axi_arvalid	(m_axi_arvalid), // Templated
							      .s_axi_rready	(m_axi_rready));	 // Templated




endmodule




// Local Variables:
// verilog-auto-inst-param-value:t
// verilog-library-directories:("." "../../rtl" "../" )
// End:







module clk_gen (  /*AUTOARG*/
    // Outputs
    clk_8x,
    clk
);

  output clk_8x;
  output clk;

  reg clk_8x;
  reg clk;

  initial begin
    clk_8x = 1;
    forever #1 clk_8x = ~clk_8x;
  end

  initial begin
    clk = 0;
    forever #8 clk = ~clk;
  end




endmodule




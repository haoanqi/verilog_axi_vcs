module tb  
(  /*autoarg*/
   // Outputs
   rst_n_8x
   );

   localparam S_COUNT = 2;
   localparam M_COUNT = 1;

    //// Width of data bus in bits
    //parameter AXI_DATA_WIDTH = 32;
    //// Width of address bus in bits
    //parameter AXI_ADDR_WIDTH = 16;
    //// Width of wstrb (width of data bus in words)
    //parameter AXI_STRB_WIDTH = (AXI_DATA_WIDTH/8);
    //// Width of AXI ID signal
    //parameter AXI_ID_WIDTH = 8;
    // Maximum AXI burst length to generate
    //parameter AXI_MAX_BURST_LEN = 16;
    // Width of length field
    //parameter LEN_WIDTH = 20;
    // Width of tag field
    //parameter TAG_WIDTH = 8;
    // Enable support for unaligned transfers
    //parameter ENABLE_UNALIGNED = 0

   // Width of AXI data bus in bits
    parameter AXI_DATA_WIDTH = 512;
    // Width of AXI address bus in bits
    parameter AXI_ADDR_WIDTH = 32;
    // Width of AXI wstrb (width of data bus in words)
    parameter AXI_STRB_WIDTH = (AXI_DATA_WIDTH/8);
    // Width of AXI ID signal
    parameter AXI_ID_WIDTH = 8;
    // Maximum AXI burst length to generate
    parameter AXI_MAX_BURST_LEN = 16;
    // Width of AXI stream interfaces in bits
    parameter AXIS_DATA_WIDTH = AXI_DATA_WIDTH;
    // Use AXI stream tkeep signal
    parameter AXIS_KEEP_ENABLE = (AXIS_DATA_WIDTH>8);
    // AXI stream tkeep signal width (words per cycle)
    parameter AXIS_KEEP_WIDTH = (AXIS_DATA_WIDTH/8);
    // Use AXI stream tlast signal
    parameter AXIS_LAST_ENABLE = 1;
    // Propagate AXI stream tid signal
    parameter AXIS_ID_ENABLE = 0;
    // AXI stream tid signal width
    parameter AXIS_ID_WIDTH = 8;
    // Propagate AXI stream tdest signal
    parameter AXIS_DEST_ENABLE = 0;
    // AXI stream tdest signal width
    parameter AXIS_DEST_WIDTH = 8;
    // Propagate AXI stream tuser signal
    parameter AXIS_USER_ENABLE = 1;
    // AXI stream tuser signal width
    parameter AXIS_USER_WIDTH = 1;
    // Width of length field
    parameter LEN_WIDTH = 20;
    // Width of tag field
    parameter TAG_WIDTH = 8;
    // Enable support for scatter/gather DMA
    // (multiple descriptors per AXI stream frame)
    parameter ENABLE_SG = 0;
    // Enable support for unaligned transfers
    parameter ENABLE_UNALIGNED = 0 ;


parameter AXI_WORD_WIDTH = AXI_STRB_WIDTH;
parameter AXI_WORD_SIZE = AXI_DATA_WIDTH/AXI_WORD_WIDTH;
parameter AXI_BURST_SIZE = $clog2(AXI_STRB_WIDTH);
parameter AXI_MAX_BURST_SIZE = AXI_MAX_BURST_LEN << AXI_BURST_SIZE;

parameter OFFSET_WIDTH = AXI_STRB_WIDTH > 1 ? $clog2(AXI_STRB_WIDTH) : 1;
parameter OFFSET_MASK = AXI_STRB_WIDTH > 1 ? {OFFSET_WIDTH{1'b1}} : 0;
parameter ADDR_MASK = {AXI_ADDR_WIDTH{1'b1}} << $clog2(AXI_STRB_WIDTH);
parameter CYCLE_COUNT_WIDTH = LEN_WIDTH - AXI_BURST_SIZE + 1;

parameter STATUS_FIFO_ADDR_WIDTH = 5;
parameter OUTPUT_FIFO_ADDR_WIDTH = 5;



   // Width of data bus in bits
    parameter DATA_WIDTH = 512;
    // Width of address bus in bits
    parameter ADDR_WIDTH = 32;
    // Width of wstrb (width of data bus in words)
    parameter STRB_WIDTH = (DATA_WIDTH/8);
    // Input ID field width (from AXI masters)
    parameter S_ID_WIDTH = 7;
    // Output ID field width (towards AXI slaves)
    // Additional bits required for response routing
    parameter M_ID_WIDTH = S_ID_WIDTH+$clog2(S_COUNT);
    // Propagate awuser signal
    parameter AWUSER_ENABLE = 0;
    // Width of awuser signal
    parameter AWUSER_WIDTH = 1;
    // Propagate wuser signal
    parameter WUSER_ENABLE = 0;
    // Width of wuser signal
    parameter WUSER_WIDTH = 1;
    // Propagate buser signal
    parameter BUSER_ENABLE = 0;
    // Width of buser signal
    parameter BUSER_WIDTH = 1;
    // Propagate aruser signal
    parameter ARUSER_ENABLE = 0;
    // Width of aruser signal
    parameter ARUSER_WIDTH = 1;
    // Propagate ruser signal
    parameter RUSER_ENABLE = 0;
    // Width of ruser signal
    parameter RUSER_WIDTH = 1;
    // Number of concurrent unique IDs
    parameter S00_THREADS = 2;
    // Number of concurrent operations
    parameter S00_ACCEPT = 16;
    // Number of concurrent unique IDs
    parameter S01_THREADS = 2;
    // Number of concurrent operations
    parameter S01_ACCEPT = 16;
    // Number of regions per master interface
    parameter M_REGIONS = 1;
    // Master interface base addresses
    // M_REGIONS concatenated fields of ADDR_WIDTH bits
    parameter M00_BASE_ADDR = 0;
    // Master interface address widths
    // M_REGIONS concatenated fields of 32 bits
    parameter M00_ADDR_WIDTH = {M_REGIONS{32'd32}};
    // Read connections between interfaces
    // S_COUNT bits
    parameter M00_CONNECT_READ = 2'b11;
    // Write connections between interfaces
    // S_COUNT bits
    parameter M00_CONNECT_WRITE = 2'b11;
    // Number of concurrent operations for each master interface
    parameter M00_ISSUE = 4;
    // Secure master (fail operations based on awprot/arprot)
    parameter M00_SECURE = 0;
    // Slave interface AW channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_AW_REG_TYPE = 0;
    // Slave interface W channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_W_REG_TYPE = 0;
    // Slave interface B channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_B_REG_TYPE = 1;
    // Slave interface AR channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_AR_REG_TYPE = 0;
    // Slave interface R channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S00_R_REG_TYPE = 2;
    // Slave interface AW channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_AW_REG_TYPE = 0;
    // Slave interface W channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_W_REG_TYPE = 0;
    // Slave interface B channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_B_REG_TYPE = 1;
    // Slave interface AR channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_AR_REG_TYPE = 0;
    // Slave interface R channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter S01_R_REG_TYPE = 2;
    // Master interface AW channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_AW_REG_TYPE = 1;
    // Master interface W channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_W_REG_TYPE = 2;
    // Master interface B channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_B_REG_TYPE = 0;
    // Master interface AR channel register type (output)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_AR_REG_TYPE = 1;
    // Master interface R channel register type (input)
    // 0 to bypass; 1 for simple buffer; 2 for skid buffer
    parameter M00_R_REG_TYPE = 0;


      // Width of data bus in bits
    //parameter DATA_WIDTH = 32;
    // Width of address bus in bits
    //parameter ADDR_WIDTH = 16;
    // Width of wstrb (width of data bus in words)
    //parameter STRB_WIDTH = (DATA_WIDTH/8);
    // Width of ID signal
    parameter ID_WIDTH = 8;
    // Extra pipeline register on output
    parameter PIPELINE_OUTPUT = 0;

parameter VALID_ADDR_WIDTH = ADDR_WIDTH - $clog2(STRB_WIDTH);
parameter WORD_WIDTH = STRB_WIDTH;
parameter WORD_SIZE = DATA_WIDTH/WORD_WIDTH;





  /*AUTOINPUT*/
  /*AUTOOUTPUT*/
  // Beginning of automatic outputs (from unused autoinst outputs)
  output		rst_n_8x;		// From u_rst_gen of rst_gen.v
  // End of automatics
  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  wire			clk;			// From u_clk_gen of clk_gen.v
  wire			clk_8x;			// From u_clk_gen of clk_gen.v
  wire			enable;			// From tx_sys of tx_sys.v
  wire [3:0]		m_axis_desc_status_error;// From u_axi_top of axi_top.v
  wire [TAG_WIDTH-1:0]	m_axis_desc_status_tag;	// From u_axi_top of axi_top.v
  wire			m_axis_desc_status_valid;// From u_axi_top of axi_top.v
  wire [AXIS_DATA_WIDTH-1:0] m_axis_read_data_tdata;// From u_axi_top of axi_top.v
  wire [AXIS_DEST_WIDTH-1:0] m_axis_read_data_tdest;// From u_axi_top of axi_top.v
  wire [AXIS_ID_WIDTH-1:0] m_axis_read_data_tid;// From u_axi_top of axi_top.v
  wire [AXIS_KEEP_WIDTH-1:0] m_axis_read_data_tkeep;// From u_axi_top of axi_top.v
  wire			m_axis_read_data_tlast;	// From u_axi_top of axi_top.v
  wire			m_axis_read_data_tready;// From tx_sys of tx_sys.v
  wire [AXIS_USER_WIDTH-1:0] m_axis_read_data_tuser;// From u_axi_top of axi_top.v
  wire			m_axis_read_data_tvalid;// From u_axi_top of axi_top.v
  wire [3:0]		m_axis_read_desc_status_error;// From u_axi_top of axi_top.v
  wire [TAG_WIDTH-1:0]	m_axis_read_desc_status_tag;// From u_axi_top of axi_top.v
  wire			m_axis_read_desc_status_valid;// From u_axi_top of axi_top.v
  wire [AXIS_DEST_WIDTH-1:0] m_axis_write_desc_status_dest;// From u_axi_top of axi_top.v
  wire [3:0]		m_axis_write_desc_status_error;// From u_axi_top of axi_top.v
  wire [AXIS_ID_WIDTH-1:0] m_axis_write_desc_status_id;// From u_axi_top of axi_top.v
  wire [LEN_WIDTH-1:0]	m_axis_write_desc_status_len;// From u_axi_top of axi_top.v
  wire [TAG_WIDTH-1:0]	m_axis_write_desc_status_tag;// From u_axi_top of axi_top.v
  wire [AXIS_USER_WIDTH-1:0] m_axis_write_desc_status_user;// From u_axi_top of axi_top.v
  wire			m_axis_write_desc_status_valid;// From u_axi_top of axi_top.v
  wire			read_enable;		// From tx_sys of tx_sys.v
  wire			rst_n;			// From u_rst_gen of rst_gen.v
  wire [LEN_WIDTH-1:0]	s_axis_desc_len;	// From tx_sys of tx_sys.v
  wire [AXI_ADDR_WIDTH-1:0] s_axis_desc_read_addr;// From tx_sys of tx_sys.v
  wire			s_axis_desc_ready;	// From u_axi_top of axi_top.v
  wire [TAG_WIDTH-1:0]	s_axis_desc_tag;	// From tx_sys of tx_sys.v
  wire			s_axis_desc_valid;	// From tx_sys of tx_sys.v
  wire [AXI_ADDR_WIDTH-1:0] s_axis_desc_write_addr;// From tx_sys of tx_sys.v
  wire [AXI_ADDR_WIDTH-1:0] s_axis_read_desc_addr;// From tx_sys of tx_sys.v
  wire [AXIS_DEST_WIDTH-1:0] s_axis_read_desc_dest;// From tx_sys of tx_sys.v
  wire [AXIS_ID_WIDTH-1:0] s_axis_read_desc_id;	// From tx_sys of tx_sys.v
  wire [LEN_WIDTH-1:0]	s_axis_read_desc_len;	// From tx_sys of tx_sys.v
  wire			s_axis_read_desc_ready;	// From u_axi_top of axi_top.v
  wire [TAG_WIDTH-1:0]	s_axis_read_desc_tag;	// From tx_sys of tx_sys.v
  wire [AXIS_USER_WIDTH-1:0] s_axis_read_desc_user;// From tx_sys of tx_sys.v
  wire			s_axis_read_desc_valid;	// From tx_sys of tx_sys.v
  wire [AXIS_DATA_WIDTH-1:0] s_axis_write_data_tdata;// From tx_sys of tx_sys.v
  wire [AXIS_DEST_WIDTH-1:0] s_axis_write_data_tdest;// From tx_sys of tx_sys.v
  wire [AXIS_ID_WIDTH-1:0] s_axis_write_data_tid;// From tx_sys of tx_sys.v
  wire [AXIS_KEEP_WIDTH-1:0] s_axis_write_data_tkeep;// From tx_sys of tx_sys.v
  wire			s_axis_write_data_tlast;// From tx_sys of tx_sys.v
  wire			s_axis_write_data_tready;// From u_axi_top of axi_top.v
  wire [AXIS_USER_WIDTH-1:0] s_axis_write_data_tuser;// From tx_sys of tx_sys.v
  wire			s_axis_write_data_tvalid;// From tx_sys of tx_sys.v
  wire [AXI_ADDR_WIDTH-1:0] s_axis_write_desc_addr;// From tx_sys of tx_sys.v
  wire [LEN_WIDTH-1:0]	s_axis_write_desc_len;	// From tx_sys of tx_sys.v
  wire			s_axis_write_desc_ready;// From u_axi_top of axi_top.v
  wire [TAG_WIDTH-1:0]	s_axis_write_desc_tag;	// From tx_sys of tx_sys.v
  wire			s_axis_write_desc_valid;// From tx_sys of tx_sys.v
  wire			write_abort;		// From tx_sys of tx_sys.v
  wire			write_enable;		// From tx_sys of tx_sys.v
  // End of automatics

  /* axi_top  AUTO_TEMPLATE (
	.rst		(~rst_n),
	.clk1		(clk_8x),
 );
*/


  axi_top #(
      /*AUTOINSTPARAM*/
	    // Parameters
	    .AXI_DATA_WIDTH		(AXI_DATA_WIDTH),
	    .AXI_ADDR_WIDTH		(AXI_ADDR_WIDTH),
	    .AXI_STRB_WIDTH		(AXI_STRB_WIDTH),
	    .AXI_ID_WIDTH		(AXI_ID_WIDTH),
	    .AXI_MAX_BURST_LEN		(AXI_MAX_BURST_LEN),
	    .AXIS_DATA_WIDTH		(AXIS_DATA_WIDTH),
	    .AXIS_KEEP_ENABLE		(AXIS_KEEP_ENABLE),
	    .AXIS_KEEP_WIDTH		(AXIS_KEEP_WIDTH),
	    .AXIS_LAST_ENABLE		(AXIS_LAST_ENABLE),
	    .AXIS_ID_ENABLE		(AXIS_ID_ENABLE),
	    .AXIS_ID_WIDTH		(AXIS_ID_WIDTH),
	    .AXIS_DEST_ENABLE		(AXIS_DEST_ENABLE),
	    .AXIS_DEST_WIDTH		(AXIS_DEST_WIDTH),
	    .AXIS_USER_ENABLE		(AXIS_USER_ENABLE),
	    .AXIS_USER_WIDTH		(AXIS_USER_WIDTH),
	    .LEN_WIDTH			(LEN_WIDTH),
	    .TAG_WIDTH			(TAG_WIDTH),
	    .ENABLE_SG			(ENABLE_SG),
	    .ENABLE_UNALIGNED		(ENABLE_UNALIGNED),
	    .AXI_WORD_WIDTH		(AXI_WORD_WIDTH),
	    .AXI_WORD_SIZE		(AXI_WORD_SIZE),
	    .AXI_BURST_SIZE		(AXI_BURST_SIZE),
	    .AXI_MAX_BURST_SIZE		(AXI_MAX_BURST_SIZE),
	    .OFFSET_WIDTH		(OFFSET_WIDTH),
	    .OFFSET_MASK		(OFFSET_MASK),
	    .ADDR_MASK			(ADDR_MASK),
	    .CYCLE_COUNT_WIDTH		(CYCLE_COUNT_WIDTH),
	    .STATUS_FIFO_ADDR_WIDTH	(STATUS_FIFO_ADDR_WIDTH),
	    .OUTPUT_FIFO_ADDR_WIDTH	(OUTPUT_FIFO_ADDR_WIDTH),
	    .DATA_WIDTH			(DATA_WIDTH),
	    .ADDR_WIDTH			(ADDR_WIDTH),
	    .STRB_WIDTH			(STRB_WIDTH),
	    .S_ID_WIDTH			(S_ID_WIDTH),
	    .M_ID_WIDTH			(M_ID_WIDTH),
	    .AWUSER_ENABLE		(AWUSER_ENABLE),
	    .AWUSER_WIDTH		(AWUSER_WIDTH),
	    .WUSER_ENABLE		(WUSER_ENABLE),
	    .WUSER_WIDTH		(WUSER_WIDTH),
	    .BUSER_ENABLE		(BUSER_ENABLE),
	    .BUSER_WIDTH		(BUSER_WIDTH),
	    .ARUSER_ENABLE		(ARUSER_ENABLE),
	    .ARUSER_WIDTH		(ARUSER_WIDTH),
	    .RUSER_ENABLE		(RUSER_ENABLE),
	    .RUSER_WIDTH		(RUSER_WIDTH),
	    .S00_THREADS		(S00_THREADS),
	    .S00_ACCEPT			(S00_ACCEPT),
	    .S01_THREADS		(S01_THREADS),
	    .S01_ACCEPT			(S01_ACCEPT),
	    .M_REGIONS			(M_REGIONS),
	    .M00_BASE_ADDR		(M00_BASE_ADDR),
	    .M00_ADDR_WIDTH		(M00_ADDR_WIDTH),
	    .M00_CONNECT_READ		(M00_CONNECT_READ),
	    .M00_CONNECT_WRITE		(M00_CONNECT_WRITE),
	    .M00_ISSUE			(M00_ISSUE),
	    .M00_SECURE			(M00_SECURE),
	    .S00_AW_REG_TYPE		(S00_AW_REG_TYPE),
	    .S00_W_REG_TYPE		(S00_W_REG_TYPE),
	    .S00_B_REG_TYPE		(S00_B_REG_TYPE),
	    .S00_AR_REG_TYPE		(S00_AR_REG_TYPE),
	    .S00_R_REG_TYPE		(S00_R_REG_TYPE),
	    .S01_AW_REG_TYPE		(S01_AW_REG_TYPE),
	    .S01_W_REG_TYPE		(S01_W_REG_TYPE),
	    .S01_B_REG_TYPE		(S01_B_REG_TYPE),
	    .S01_AR_REG_TYPE		(S01_AR_REG_TYPE),
	    .S01_R_REG_TYPE		(S01_R_REG_TYPE),
	    .M00_AW_REG_TYPE		(M00_AW_REG_TYPE),
	    .M00_W_REG_TYPE		(M00_W_REG_TYPE),
	    .M00_B_REG_TYPE		(M00_B_REG_TYPE),
	    .M00_AR_REG_TYPE		(M00_AR_REG_TYPE),
	    .M00_R_REG_TYPE		(M00_R_REG_TYPE),
	    .ID_WIDTH			(ID_WIDTH),
	    .PIPELINE_OUTPUT		(PIPELINE_OUTPUT),
	    .VALID_ADDR_WIDTH		(VALID_ADDR_WIDTH),
	    .WORD_WIDTH			(WORD_WIDTH),
	    .WORD_SIZE			(WORD_SIZE)) u_axi_top (  /*autoinst*/
								// Outputs
								.m_axis_desc_status_error(m_axis_desc_status_error[3:0]),
								.m_axis_desc_status_tag(m_axis_desc_status_tag[TAG_WIDTH-1:0]),
								.m_axis_desc_status_valid(m_axis_desc_status_valid),
								.m_axis_read_data_tdata(m_axis_read_data_tdata[AXIS_DATA_WIDTH-1:0]),
								.m_axis_read_data_tdest(m_axis_read_data_tdest[AXIS_DEST_WIDTH-1:0]),
								.m_axis_read_data_tid(m_axis_read_data_tid[AXIS_ID_WIDTH-1:0]),
								.m_axis_read_data_tkeep(m_axis_read_data_tkeep[AXIS_KEEP_WIDTH-1:0]),
								.m_axis_read_data_tlast(m_axis_read_data_tlast),
								.m_axis_read_data_tuser(m_axis_read_data_tuser[AXIS_USER_WIDTH-1:0]),
								.m_axis_read_data_tvalid(m_axis_read_data_tvalid),
								.m_axis_read_desc_status_error(m_axis_read_desc_status_error[3:0]),
								.m_axis_read_desc_status_tag(m_axis_read_desc_status_tag[TAG_WIDTH-1:0]),
								.m_axis_read_desc_status_valid(m_axis_read_desc_status_valid),
								.m_axis_write_desc_status_dest(m_axis_write_desc_status_dest[AXIS_DEST_WIDTH-1:0]),
								.m_axis_write_desc_status_error(m_axis_write_desc_status_error[3:0]),
								.m_axis_write_desc_status_id(m_axis_write_desc_status_id[AXIS_ID_WIDTH-1:0]),
								.m_axis_write_desc_status_len(m_axis_write_desc_status_len[LEN_WIDTH-1:0]),
								.m_axis_write_desc_status_tag(m_axis_write_desc_status_tag[TAG_WIDTH-1:0]),
								.m_axis_write_desc_status_user(m_axis_write_desc_status_user[AXIS_USER_WIDTH-1:0]),
								.m_axis_write_desc_status_valid(m_axis_write_desc_status_valid),
								.s_axis_desc_ready(s_axis_desc_ready),
								.s_axis_read_desc_ready(s_axis_read_desc_ready),
								.s_axis_write_data_tready(s_axis_write_data_tready),
								.s_axis_write_desc_ready(s_axis_write_desc_ready),
								// Inputs
								.clk		(clk),
								.clk1		(clk_8x),	 // Templated
								.enable		(enable),
								.m_axis_read_data_tready(m_axis_read_data_tready),
								.read_enable	(read_enable),
								.rst		(~rst_n),	 // Templated
								.s_axis_desc_len(s_axis_desc_len[LEN_WIDTH-1:0]),
								.s_axis_desc_read_addr(s_axis_desc_read_addr[AXI_ADDR_WIDTH-1:0]),
								.s_axis_desc_tag(s_axis_desc_tag[TAG_WIDTH-1:0]),
								.s_axis_desc_valid(s_axis_desc_valid),
								.s_axis_desc_write_addr(s_axis_desc_write_addr[AXI_ADDR_WIDTH-1:0]),
								.s_axis_read_desc_addr(s_axis_read_desc_addr[AXI_ADDR_WIDTH-1:0]),
								.s_axis_read_desc_dest(s_axis_read_desc_dest[AXIS_DEST_WIDTH-1:0]),
								.s_axis_read_desc_id(s_axis_read_desc_id[AXIS_ID_WIDTH-1:0]),
								.s_axis_read_desc_len(s_axis_read_desc_len[LEN_WIDTH-1:0]),
								.s_axis_read_desc_tag(s_axis_read_desc_tag[TAG_WIDTH-1:0]),
								.s_axis_read_desc_user(s_axis_read_desc_user[AXIS_USER_WIDTH-1:0]),
								.s_axis_read_desc_valid(s_axis_read_desc_valid),
								.s_axis_write_data_tdata(s_axis_write_data_tdata[AXIS_DATA_WIDTH-1:0]),
								.s_axis_write_data_tdest(s_axis_write_data_tdest[AXIS_DEST_WIDTH-1:0]),
								.s_axis_write_data_tid(s_axis_write_data_tid[AXIS_ID_WIDTH-1:0]),
								.s_axis_write_data_tkeep(s_axis_write_data_tkeep[AXIS_KEEP_WIDTH-1:0]),
								.s_axis_write_data_tlast(s_axis_write_data_tlast),
								.s_axis_write_data_tuser(s_axis_write_data_tuser[AXIS_USER_WIDTH-1:0]),
								.s_axis_write_data_tvalid(s_axis_write_data_tvalid),
								.s_axis_write_desc_addr(s_axis_write_desc_addr[AXI_ADDR_WIDTH-1:0]),
								.s_axis_write_desc_len(s_axis_write_desc_len[LEN_WIDTH-1:0]),
								.s_axis_write_desc_tag(s_axis_write_desc_tag[TAG_WIDTH-1:0]),
								.s_axis_write_desc_valid(s_axis_write_desc_valid),
								.write_abort	(write_abort),
								.write_enable	(write_enable));



  tx_sys #(
      /*AUTOINSTPARAM*/
	   // Parameters
	   .AXI_DATA_WIDTH		(AXI_DATA_WIDTH),
	   .AXI_ADDR_WIDTH		(AXI_ADDR_WIDTH),
	   .AXI_STRB_WIDTH		(AXI_STRB_WIDTH),
	   .AXI_ID_WIDTH		(AXI_ID_WIDTH),
	   .AXI_MAX_BURST_LEN		(AXI_MAX_BURST_LEN),
	   .AXIS_DATA_WIDTH		(AXIS_DATA_WIDTH),
	   .AXIS_KEEP_ENABLE		(AXIS_KEEP_ENABLE),
	   .AXIS_KEEP_WIDTH		(AXIS_KEEP_WIDTH),
	   .AXIS_LAST_ENABLE		(AXIS_LAST_ENABLE),
	   .AXIS_ID_ENABLE		(AXIS_ID_ENABLE),
	   .AXIS_ID_WIDTH		(AXIS_ID_WIDTH),
	   .AXIS_DEST_ENABLE		(AXIS_DEST_ENABLE),
	   .AXIS_DEST_WIDTH		(AXIS_DEST_WIDTH),
	   .AXIS_USER_ENABLE		(AXIS_USER_ENABLE),
	   .AXIS_USER_WIDTH		(AXIS_USER_WIDTH),
	   .LEN_WIDTH			(LEN_WIDTH),
	   .TAG_WIDTH			(TAG_WIDTH),
	   .ENABLE_SG			(ENABLE_SG),
	   .ENABLE_UNALIGNED		(ENABLE_UNALIGNED)) tx_sys (  /*autoinst*/
								    // Outputs
								    .enable		(enable),
								    .m_axis_read_data_tready(m_axis_read_data_tready),
								    .read_enable	(read_enable),
								    .s_axis_desc_len	(s_axis_desc_len[LEN_WIDTH-1:0]),
								    .s_axis_desc_read_addr(s_axis_desc_read_addr[AXI_ADDR_WIDTH-1:0]),
								    .s_axis_desc_tag	(s_axis_desc_tag[TAG_WIDTH-1:0]),
								    .s_axis_desc_valid	(s_axis_desc_valid),
								    .s_axis_desc_write_addr(s_axis_desc_write_addr[AXI_ADDR_WIDTH-1:0]),
								    .s_axis_read_desc_addr(s_axis_read_desc_addr[AXI_ADDR_WIDTH-1:0]),
								    .s_axis_read_desc_dest(s_axis_read_desc_dest[AXIS_DEST_WIDTH-1:0]),
								    .s_axis_read_desc_id(s_axis_read_desc_id[AXIS_ID_WIDTH-1:0]),
								    .s_axis_read_desc_len(s_axis_read_desc_len[LEN_WIDTH-1:0]),
								    .s_axis_read_desc_tag(s_axis_read_desc_tag[TAG_WIDTH-1:0]),
								    .s_axis_read_desc_user(s_axis_read_desc_user[AXIS_USER_WIDTH-1:0]),
								    .s_axis_read_desc_valid(s_axis_read_desc_valid),
								    .s_axis_write_data_tdata(s_axis_write_data_tdata[AXIS_DATA_WIDTH-1:0]),
								    .s_axis_write_data_tdest(s_axis_write_data_tdest[AXIS_DEST_WIDTH-1:0]),
								    .s_axis_write_data_tid(s_axis_write_data_tid[AXIS_ID_WIDTH-1:0]),
								    .s_axis_write_data_tkeep(s_axis_write_data_tkeep[AXIS_KEEP_WIDTH-1:0]),
								    .s_axis_write_data_tlast(s_axis_write_data_tlast),
								    .s_axis_write_data_tuser(s_axis_write_data_tuser[AXIS_USER_WIDTH-1:0]),
								    .s_axis_write_data_tvalid(s_axis_write_data_tvalid),
								    .s_axis_write_desc_addr(s_axis_write_desc_addr[AXI_ADDR_WIDTH-1:0]),
								    .s_axis_write_desc_len(s_axis_write_desc_len[LEN_WIDTH-1:0]),
								    .s_axis_write_desc_tag(s_axis_write_desc_tag[TAG_WIDTH-1:0]),
								    .s_axis_write_desc_valid(s_axis_write_desc_valid),
								    .write_abort	(write_abort),
								    .write_enable	(write_enable),
								    // Inputs
								    .clk		(clk),
								    .rst_n		(rst_n),
								    .m_axis_desc_status_error(m_axis_desc_status_error[3:0]),
								    .m_axis_desc_status_tag(m_axis_desc_status_tag[TAG_WIDTH-1:0]),
								    .m_axis_desc_status_valid(m_axis_desc_status_valid),
								    .m_axis_read_data_tdata(m_axis_read_data_tdata[AXIS_DATA_WIDTH-1:0]),
								    .m_axis_read_data_tdest(m_axis_read_data_tdest[AXIS_DEST_WIDTH-1:0]),
								    .m_axis_read_data_tid(m_axis_read_data_tid[AXIS_ID_WIDTH-1:0]),
								    .m_axis_read_data_tkeep(m_axis_read_data_tkeep[AXIS_KEEP_WIDTH-1:0]),
								    .m_axis_read_data_tlast(m_axis_read_data_tlast),
								    .m_axis_read_data_tuser(m_axis_read_data_tuser[AXIS_USER_WIDTH-1:0]),
								    .m_axis_read_data_tvalid(m_axis_read_data_tvalid),
								    .m_axis_read_desc_status_error(m_axis_read_desc_status_error[3:0]),
								    .m_axis_read_desc_status_tag(m_axis_read_desc_status_tag[TAG_WIDTH-1:0]),
								    .m_axis_read_desc_status_valid(m_axis_read_desc_status_valid),
								    .m_axis_write_desc_status_dest(m_axis_write_desc_status_dest[AXIS_DEST_WIDTH-1:0]),
								    .m_axis_write_desc_status_error(m_axis_write_desc_status_error[3:0]),
								    .m_axis_write_desc_status_id(m_axis_write_desc_status_id[AXIS_ID_WIDTH-1:0]),
								    .m_axis_write_desc_status_len(m_axis_write_desc_status_len[LEN_WIDTH-1:0]),
								    .m_axis_write_desc_status_tag(m_axis_write_desc_status_tag[TAG_WIDTH-1:0]),
								    .m_axis_write_desc_status_user(m_axis_write_desc_status_user[AXIS_USER_WIDTH-1:0]),
								    .m_axis_write_desc_status_valid(m_axis_write_desc_status_valid),
								    .s_axis_desc_ready	(s_axis_desc_ready),
								    .s_axis_read_desc_ready(s_axis_read_desc_ready),
								    .s_axis_write_data_tready(s_axis_write_data_tready),
								    .s_axis_write_desc_ready(s_axis_write_desc_ready));


  ini_mem _ini_mem (  /*autoinst*/);






  clk_gen u_clk_gen (  /*AUTOINST*/
		     // Outputs
		     .clk_8x		(clk_8x),
		     .clk		(clk));

  rst_gen u_rst_gen (  /*AUTOINST*/
		     // Outputs
		     .rst_n_8x		(rst_n_8x),
		     .rst_n		(rst_n));

  dump dump ();


  tx_mon tx_mon ();

  forcelist forcelist (  /*AUTOINST*/);



endmodule


// Local Variables:
// verilog-library-directories:("." "../rtl"  "../../rtl" )
// End:










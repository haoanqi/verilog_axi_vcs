module rst_gen (  /*AUTOARG*/
    // Outputs
    rst_n_8x,
    rst_n
);

  output rst_n_8x;
  output rst_n;
  reg rst_n_8x;
  reg rst_n;

  initial begin
    rst_n_8x = 1'b0;
    rst_n = 1'b0;
    #1000 rst_n_8x = 1'b1;
    rst_n = 1'b1;
  end

endmodule



module tx_sys  (  /*autoarg*/
   // Outputs
   enable, m_axis_read_data_tready, read_enable, s_axis_desc_len,
   s_axis_desc_read_addr, s_axis_desc_tag, s_axis_desc_valid,
   s_axis_desc_write_addr, s_axis_read_desc_addr,
   s_axis_read_desc_dest, s_axis_read_desc_id, s_axis_read_desc_len,
   s_axis_read_desc_tag, s_axis_read_desc_user,
   s_axis_read_desc_valid, s_axis_write_data_tdata,
   s_axis_write_data_tdest, s_axis_write_data_tid,
   s_axis_write_data_tkeep, s_axis_write_data_tlast,
   s_axis_write_data_tuser, s_axis_write_data_tvalid,
   s_axis_write_desc_addr, s_axis_write_desc_len,
   s_axis_write_desc_tag, s_axis_write_desc_valid, write_abort,
   write_enable,
   // Inputs
   clk, rst_n, m_axis_desc_status_error, m_axis_desc_status_tag,
   m_axis_desc_status_valid, m_axis_read_data_tdata,
   m_axis_read_data_tdest, m_axis_read_data_tid,
   m_axis_read_data_tkeep, m_axis_read_data_tlast,
   m_axis_read_data_tuser, m_axis_read_data_tvalid,
   m_axis_read_desc_status_error, m_axis_read_desc_status_tag,
   m_axis_read_desc_status_valid, m_axis_write_desc_status_dest,
   m_axis_write_desc_status_error, m_axis_write_desc_status_id,
   m_axis_write_desc_status_len, m_axis_write_desc_status_tag,
   m_axis_write_desc_status_user, m_axis_write_desc_status_valid,
   s_axis_desc_ready, s_axis_read_desc_ready,
   s_axis_write_data_tready, s_axis_write_desc_ready
   );



  // Width of AXI data bus in bits
    parameter AXI_DATA_WIDTH = 512;
    // Width of AXI address bus in bits
    parameter AXI_ADDR_WIDTH = 32;
    // Width of AXI wstrb (width of data bus in words)
    parameter AXI_STRB_WIDTH = (AXI_DATA_WIDTH/8);
    // Width of AXI ID signal
    parameter AXI_ID_WIDTH = 8;
    // Maximum AXI burst length to generate
    parameter AXI_MAX_BURST_LEN = 16;
    // Width of AXI stream interfaces in bits
    parameter AXIS_DATA_WIDTH = AXI_DATA_WIDTH;
    // Use AXI stream tkeep signal
    parameter AXIS_KEEP_ENABLE = (AXIS_DATA_WIDTH>8);
    // AXI stream tkeep signal width (words per cycle)
    parameter AXIS_KEEP_WIDTH = (AXIS_DATA_WIDTH/8);
    // Use AXI stream tlast signal
    parameter AXIS_LAST_ENABLE = 1;
    // Propagate AXI stream tid signal
    parameter AXIS_ID_ENABLE = 0;
    // AXI stream tid signal width
    parameter AXIS_ID_WIDTH = 8;
    // Propagate AXI stream tdest signal
    parameter AXIS_DEST_ENABLE = 0;
    // AXI stream tdest signal width
    parameter AXIS_DEST_WIDTH = 8;
    // Propagate AXI stream tuser signal
    parameter AXIS_USER_ENABLE = 1;
    // AXI stream tuser signal width
    parameter AXIS_USER_WIDTH = 1;
    // Width of length field
    parameter LEN_WIDTH = 20;
    // Width of tag field
    parameter TAG_WIDTH = 8;
    // Enable support for scatter/gather DMA
    // (multiple descriptors per AXI stream frame)
    parameter ENABLE_SG = 0;
    // Enable support for unaligned transfers
    parameter ENABLE_UNALIGNED = 0 ;



  input clk;  // To u_CH_DIN of CH_DIN.v
  input rst_n;  // To u_CH_DIN of CH_DIN.v


  output			enable;			// To u_ddc_top of axi_top.v
  output			m_axis_read_data_tready;// To u_ddc_top of axi_top.v
  output			read_enable;		// To u_ddc_top of axi_top.v
  output [LEN_WIDTH-1:0]	s_axis_desc_len;	// To u_ddc_top of axi_top.v
  output [AXI_ADDR_WIDTH-1:0] s_axis_desc_read_addr;// To u_ddc_top of axi_top.v
  output [TAG_WIDTH-1:0]	s_axis_desc_tag;	// To u_ddc_top of axi_top.v
  output			s_axis_desc_valid;	// To u_ddc_top of axi_top.v
  output [AXI_ADDR_WIDTH-1:0] s_axis_desc_write_addr;// To u_ddc_top of axi_top.v
  output [AXI_ADDR_WIDTH-1:0] s_axis_read_desc_addr;// To u_ddc_top of axi_top.v
  output [AXIS_DEST_WIDTH-1:0] s_axis_read_desc_dest;// To u_ddc_top of axi_top.v
  output [AXIS_ID_WIDTH-1:0] s_axis_read_desc_id;// To u_ddc_top of axi_top.v
  output [LEN_WIDTH-1:0]	s_axis_read_desc_len;	// To u_ddc_top of axi_top.v
  output [TAG_WIDTH-1:0]	s_axis_read_desc_tag;	// To u_ddc_top of axi_top.v
  output [AXIS_USER_WIDTH-1:0] s_axis_read_desc_user;// To u_ddc_top of axi_top.v
  output			s_axis_read_desc_valid;	// To u_ddc_top of axi_top.v
  output [AXIS_DATA_WIDTH-1:0] s_axis_write_data_tdata;// To u_ddc_top of axi_top.v
  output [AXIS_DEST_WIDTH-1:0] s_axis_write_data_tdest;// To u_ddc_top of axi_top.v
  output [AXIS_ID_WIDTH-1:0] s_axis_write_data_tid;// To u_ddc_top of axi_top.v
  output [AXIS_KEEP_WIDTH-1:0] s_axis_write_data_tkeep;// To u_ddc_top of axi_top.v
  output			s_axis_write_data_tlast;// To u_ddc_top of axi_top.v
  output [AXIS_USER_WIDTH-1:0] s_axis_write_data_tuser;// To u_ddc_top of axi_top.v
  output			s_axis_write_data_tvalid;// To u_ddc_top of axi_top.v
  output [AXI_ADDR_WIDTH-1:0] s_axis_write_desc_addr;// To u_ddc_top of axi_top.v
  output [LEN_WIDTH-1:0]	s_axis_write_desc_len;	// To u_ddc_top of axi_top.v
  output [TAG_WIDTH-1:0]	s_axis_write_desc_tag;	// To u_ddc_top of axi_top.v
  output			s_axis_write_desc_valid;// To u_ddc_top of axi_top.v
  output			write_abort;		// To u_ddc_top of axi_top.v
  output			write_enable;		// To u_ddc_top of axi_top.v


  input [3:0]		m_axis_desc_status_error;// From u_ddc_top of axi_top.v
  input [TAG_WIDTH-1:0] m_axis_desc_status_tag;// From u_ddc_top of axi_top.v
  input		m_axis_desc_status_valid;// From u_ddc_top of axi_top.v
  input [AXIS_DATA_WIDTH-1:0] m_axis_read_data_tdata;// From u_ddc_top of axi_top.v
  input [AXIS_DEST_WIDTH-1:0] m_axis_read_data_tdest;// From u_ddc_top of axi_top.v
  input [AXIS_ID_WIDTH-1:0] m_axis_read_data_tid;// From u_ddc_top of axi_top.v
  input [AXIS_KEEP_WIDTH-1:0] m_axis_read_data_tkeep;// From u_ddc_top of axi_top.v
  input		m_axis_read_data_tlast;	// From u_ddc_top of axi_top.v
  input [AXIS_USER_WIDTH-1:0] m_axis_read_data_tuser;// From u_ddc_top of axi_top.v
  input		m_axis_read_data_tvalid;// From u_ddc_top of axi_top.v
  input [3:0]		m_axis_read_desc_status_error;// From u_ddc_top of axi_top.v
  input [TAG_WIDTH-1:0] m_axis_read_desc_status_tag;// From u_ddc_top of axi_top.v
  input		m_axis_read_desc_status_valid;// From u_ddc_top of axi_top.v
  input [AXIS_DEST_WIDTH-1:0] m_axis_write_desc_status_dest;// From u_ddc_top of axi_top.v
  input [3:0]		m_axis_write_desc_status_error;// From u_ddc_top of axi_top.v
  input [AXIS_ID_WIDTH-1:0] m_axis_write_desc_status_id;// From u_ddc_top of axi_top.v
  input [LEN_WIDTH-1:0] m_axis_write_desc_status_len;// From u_ddc_top of axi_top.v
  input [TAG_WIDTH-1:0] m_axis_write_desc_status_tag;// From u_ddc_top of axi_top.v
  input [AXIS_USER_WIDTH-1:0] m_axis_write_desc_status_user;// From u_ddc_top of axi_top.v
  input		m_axis_write_desc_status_valid;// From u_ddc_top of axi_top.v
  input		s_axis_desc_ready;	// From u_ddc_top of axi_top.v
  input		s_axis_read_desc_ready;	// From u_ddc_top of axi_top.v
  input		s_axis_write_data_tready;// From u_ddc_top of axi_top.v
  input		s_axis_write_desc_ready;// From u_ddc_top of axi_top.v



  
  reg    			enable;			// To u_ddc_top of axi_top.v
  reg    			m_axis_read_data_tready;// To u_ddc_top of axi_top.v
  reg    			read_enable;		// To u_ddc_top of axi_top.v
  reg     [LEN_WIDTH-1:0]	s_axis_desc_len;	// To u_ddc_top of axi_top.v
  reg     [AXI_ADDR_WIDTH-1:0] s_axis_desc_read_addr;// To u_ddc_top of axi_top.v
  reg     [TAG_WIDTH-1:0]	s_axis_desc_tag;	// To u_ddc_top of axi_top.v
  reg    			s_axis_desc_valid;	// To u_ddc_top of axi_top.v
  reg     [AXI_ADDR_WIDTH-1:0] s_axis_desc_write_addr;// To u_ddc_top of axi_top.v
  reg     [AXI_ADDR_WIDTH-1:0] s_axis_read_desc_addr;// To u_ddc_top of axi_top.v
  reg     [AXIS_DEST_WIDTH-1:0] s_axis_read_desc_dest;// To u_ddc_top of axi_top.v
  reg     [AXIS_ID_WIDTH-1:0] s_axis_read_desc_id;// To u_ddc_top of axi_top.v
  reg     [LEN_WIDTH-1:0]	s_axis_read_desc_len;	// To u_ddc_top of axi_top.v
  reg     [TAG_WIDTH-1:0]	s_axis_read_desc_tag;	// To u_ddc_top of axi_top.v
  reg     [AXIS_USER_WIDTH-1:0] s_axis_read_desc_user;// To u_ddc_top of axi_top.v
  reg    			s_axis_read_desc_valid;	// To u_ddc_top of axi_top.v
  reg     [AXIS_DATA_WIDTH-1:0] s_axis_write_data_tdata;// To u_ddc_top of axi_top.v
  reg     [AXIS_DEST_WIDTH-1:0] s_axis_write_data_tdest;// To u_ddc_top of axi_top.v
  reg     [AXIS_ID_WIDTH-1:0] s_axis_write_data_tid;// To u_ddc_top of axi_top.v
  reg     [AXIS_KEEP_WIDTH-1:0] s_axis_write_data_tkeep;// To u_ddc_top of axi_top.v
  reg    			s_axis_write_data_tlast;// To u_ddc_top of axi_top.v
  reg     [AXIS_USER_WIDTH-1:0] s_axis_write_data_tuser;// To u_ddc_top of axi_top.v
  reg    			s_axis_write_data_tvalid;// To u_ddc_top of axi_top.v
  reg     [AXI_ADDR_WIDTH-1:0] s_axis_write_desc_addr;// To u_ddc_top of axi_top.v
  reg     [LEN_WIDTH-1:0]	s_axis_write_desc_len;	// To u_ddc_top of axi_top.v
  reg     [TAG_WIDTH-1:0]	s_axis_write_desc_tag;	// To u_ddc_top of axi_top.v
  reg    			s_axis_write_desc_valid;// To u_ddc_top of axi_top.v
  reg    			write_abort;		// To u_ddc_top of axi_top.v
  reg    			write_enable;		// To u_ddc_top of axi_top.v


//  input params_dsp_rst;
//
//  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//  reg     [ADC_DWTH-1:0] ADCIN;
//  //reg                   ADCVLD;
//
//
//  integer                fp_r;
//  integer                count;
//
//  reg     [ADC_DWTH-1:0] ADCIN_reg;
//
//
//  initial begin
//    fp_r  = $fopen("./adc_data.txt", "r");
//    count = $fscanf(fp_r, "%d", ADCIN);
//
//    count = $fscanf(fp_r, "%d", ADCIN_reg);
//
//  end
//
//
//
//  //////////////////////////////////////////////////////////////////////////////////////////////////////////
//  always @(posedge clk_8x or negedge rst_n_8x) begin
//    if (~rst_n_8x || params_dsp_rst) begin
//    end else begin
//      ADCIN <= ADCIN_reg;
//      #0.1  
//	 count = $fscanf(fp_r, "%d", ADCIN_reg);
//    end
//  end
//


wire   rdma_done =  m_axis_read_data_tready && m_axis_read_data_tlast && m_axis_read_data_tvalid ;




reg    [15:0]    rlen ;
reg    [15:0]    wlen ;
reg    [15:0]    wcnt ;
reg    [15:0]    wmax ;

initial begin 

  enable='d0;
  m_axis_read_data_tready='d0;
  read_enable='d0;
  s_axis_desc_len='d0;
  s_axis_desc_read_addr='d0;
  s_axis_desc_tag='d0;
  s_axis_desc_valid='d0;
  s_axis_desc_write_addr='d0;
  s_axis_read_desc_addr='d0;
  s_axis_read_desc_dest='d0;
  s_axis_read_desc_id='d0;
  s_axis_read_desc_len='d0;
  s_axis_read_desc_tag='d0;
  s_axis_read_desc_user='d0;
  s_axis_read_desc_valid='d0;
  s_axis_write_data_tdata='d0;
  s_axis_write_data_tdest='d0;
  s_axis_write_data_tid='d0;
  s_axis_write_data_tkeep='d0;
  s_axis_write_data_tlast='d0;
  s_axis_write_data_tuser='d0;
  s_axis_write_data_tvalid='d0;
  s_axis_write_desc_addr='d0;
  s_axis_write_desc_len='d0;
  s_axis_write_desc_tag='d0;
  s_axis_write_desc_valid='d0;
  write_abort='d0;
  write_enable='d0;
  wcnt='d0 ;

  rlen = 64*16 ;
  wlen = 64*16 ;
  wmax =  wlen/64 + |wlen[5:0] ;

  //wait rest 
  wait (rst_n) ;

  #1000;

  //sdma-write
  write_enable='d1;  
  @(posedge clk) ;  #1;
  s_axis_write_desc_len = 64*16;
  s_axis_write_desc_addr = 64  ;
  s_axis_write_desc_tag = 1    ;
  s_axis_write_desc_valid = 1  ;

  while(1) begin 
    @(posedge clk) ;    
    if (s_axis_write_desc_ready) begin
        #1 
        s_axis_write_desc_valid = 0 ;
        break;
    end
  end

  s_axis_write_data_tdata={32{wcnt}};
  s_axis_write_data_tdest='d0;
  s_axis_write_data_tid='d1;
  s_axis_write_data_tkeep={64{1'b1}};
  s_axis_write_data_tlast=(wmax=='d1);
  s_axis_write_data_tuser='d0;
  s_axis_write_data_tvalid='d1;


   while(1) begin 
    @(posedge clk) ;    
    if (s_axis_write_data_tready) begin
        $display("%m: wdma-write_data:%h  ",s_axis_write_data_tdata);
        #1 
        wcnt = wcnt + 1 ;
        s_axis_write_data_tdata={32{wcnt}};
        if (wcnt==wmax) begin 
            s_axis_write_data_tvalid = 0 ;
            s_axis_write_data_tlast  = 0 ;
            break;
        end
        if (wcnt+1==wmax) begin 
            s_axis_write_data_tlast  = 1 ;
        end
    end
  end

  @(posedge clk) ;  
  #1
  s_axis_write_data_tlast  = 0 ;

  wait(m_axis_write_desc_status_valid);
  $display("%m: write_dma test done !!!!! ");

  ////////////////////////////////////////////////////////////////////////////////////////
  //sdma-read
   @(posedge clk) ; #1;
   m_axis_read_data_tready = 1 ;


  read_enable='d1;
   @(posedge clk) ;  #1;
  s_axis_read_desc_len = 64*16;
  s_axis_read_desc_addr = 64  ;
  s_axis_read_desc_id  = 1    ;
  s_axis_read_desc_tag = 1    ;
  s_axis_read_desc_valid = 1  ;

  while(1) begin 
   @(posedge clk) ;    
   if (s_axis_read_desc_ready) begin
        #1 
        s_axis_read_desc_valid = 0 ; 
        break;
    end
  end


  //wait (rdma_done);
  // m_axis_read_data_tready && m_axis_read_data_tlast && m_axis_read_data_tvalid ;

  while(1) begin 
   @(posedge clk) ;    
   if (  m_axis_read_data_tready &&  m_axis_read_data_tvalid   ) begin
        $display("%m: rdma-read_data:%h ",m_axis_read_data_tdata);
        if ( m_axis_read_data_tlast )
            break;
    end
  end

  $display("%m: read_dma test done !!!!!  ");
  ////////////////////////////////////////////////////////////////////////////////////////

   @(posedge clk) ;  #1;
  //cdma
  enable='d1;
  s_axis_desc_len=rlen;
  s_axis_desc_read_addr= 64 ;
  s_axis_desc_tag='d1;
  s_axis_desc_valid='d1;
  s_axis_desc_write_addr= 1088 ;
 
  while(1) begin 
   @(posedge clk) ;    
   if (s_axis_desc_ready) begin
        #1 
        s_axis_desc_valid = 0 ; 
        break;
    end
  end

  wait (m_axis_desc_status_valid);
  $display("%m: cdma test0 done !!!!!  ");



     @(posedge clk) ;  #1;
  //cdma
  enable='d1;
  s_axis_desc_len=rlen;
  s_axis_desc_read_addr= 64 ;
  s_axis_desc_tag='d1;
  s_axis_desc_valid='d1;
  s_axis_desc_write_addr= 32'hffff_0000 ;
 
  while(1) begin 
   @(posedge clk) ;    
   if (s_axis_desc_ready) begin
        #1 
        s_axis_desc_valid = 0 ; 
        break;
    end
  end

  wait (m_axis_desc_status_valid);
  $display("%m: cdma test0 done !!!!!  ");

  #1000 ;
  
  $finish; 



   







end






















endmodule


